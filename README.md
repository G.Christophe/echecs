# Echecs

Jeu d'échecs développé en Java et jouable sur un même écran (PvP). L'objectif est de découvrir le langage et le développement en général avant d'apprendre la conception et le graphisme en Java en DUT informatique 1re année.

# Règles du jeu

## Le déplacement des pièces :

- **Les pions** : Le pion avance uniquement devant lui d’une seule case. Lors de son premier déplacement, il peut exceptionnellement avancer d’une ou de deux cases. Pour prendre une pièce ennemie, il se déplace seulement en diagonale d’une seule case.

- **La tour** : La tour se déplace horizontalement ou verticalement d’autant de cases que vous le souhaitez.

- **Le cavalier** : Le cavalier saute par dessus les cases qui entourent sa position initiale (même s’il y a des pièces du même joueur) pour se poser sur une des cases qui lui est possible, c'est-à-dire, une des cases de couleur opposée, accessible en un déplacement vertical de 2 cases + un déplacement horizontal de 1 case, ou en un déplacement horizontal de 2 cases + un déplacement vertical de 1 case.

- **Le fou** : Le fou se déplace diagonalement d’autant de cases que vous le souhaitez.

- **La reine** : La reine se déplace diagonalement, horizontalement ou verticalement d’autant de cases que vous le souhaitez.

- **Le roi** : Le roi se déplace sur n’importe quelle case adjacente à sa position initiale.

## Les coups spéciaux :

- **Le roque** : Le roque permet de déplacer le roi de 2 cases vers une des 2 tours et de positionner la tour en question derrière le roi. Il ne peut plus être effectué si le roi ou la tour en question ont effectué un déplacement plus tôt dans la partie. Il est momentanément empêché si le roi est échec, ou si les cases de déplacement du roi sont contrôlées, ou s’il y a des pièces entre le roi et la tour.

- **La promotion** : Lorsqu'un pion parvient jusqu’à la dernière rangée, alors il se transforme en reine, en tour, en fou ou en cavalier. La pièce promue est immédiatement opérationnelle.

- **La prise en passant** : Lorsqu’un pion (ex: blanc) se déplace de deux cases (au cours de son premier déplacement), le joueur adverse peut le prendre "en passant" avec le pion (ex: noir) situé juste à côté du pion blanc en se déplacant juste derrière ce dernier. Ce coup n'est plus valable dans le futur si le joueur adverse décide d'effectuer un autre coup. Mais l'occasion peut éventuellement se représenter.

## Le déroulement du jeu :

Au début d’une partie, ce sont toujours les blancs qui commencent. Lors d’un tour de jeu, le joueur peut effectuer soit un déplacement, soit une prise soit un coup spécial.

## Le but du jeu :

- **Échec** : Chaque fois que vous pouvez prendre le roi de votre adversaire à votre prochain coup, vous devez le prévenir en annonçant "échec". Il doit alors tenter de sauver son roi lors de son tour.

- **Échec et mat** : Si vous avez annoncé "échec" et qu'il ne parvient pas à sauver son roi, alors il est "échec et mat". Vous remportez la partie.

- **Pat** : Une partie peut également se terminer sur un match nul. Si lors du tour d’un joueur, celui-ci ne peut déplacer aucune de ses pièces et que son roi n’est pas en échec, on dit alors que la partie se termine par un « pat ».

# Good Luck. Have Fun.
