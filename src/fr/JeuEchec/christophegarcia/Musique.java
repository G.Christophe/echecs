package fr.JeuEchec.christophegarcia;

import java.applet.AudioClip;
import java.io.File;
import java.net.URL;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
 
public class Musique extends Thread {
    private URL url;
    private AudioClip sound;
    
    public Musique() {
    	// Musique de fond.
    	try {
    		Clip clip = AudioSystem.getClip();
    		clip.open(AudioSystem.getAudioInputStream(new File("musiquePrincipale.wav")));
    		clip.start();
    		//Thread.sleep(clip.getMicrosecondLength() / 1000);
    	} catch(Exception e1) {
    		e1.getStackTrace();
    	}
    }
    /*
    public void jouer() {
        sound.play();
    }
    
    public void jouerEnBoucle() {
        sound.loop();
    }
    
    public void arreter() {
        sound.stop();
    }*/
}
