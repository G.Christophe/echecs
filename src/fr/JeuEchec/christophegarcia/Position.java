package fr.JeuEchec.christophegarcia;

public class Position {
	int colonne; // coordonnee X de la piece 
	int ligne; // coordonnee Y de la piece 
	
	public Position(int colonne, int ligne) {
		setColonne(colonne);
		setLigne(ligne);
	}
	
	public int getColonne() {
		return colonne;
	}
	
	public int getLigne() {
		return ligne;
	}
	
	public void setColonne(int colonne) {
		if(colonne >= 0 && colonne <= 7) {
			this.colonne = colonne;
		}
	}
	
	public void setLigne(int ligne) {
		if(ligne >= 0 && ligne <= 7) {
			this.ligne = ligne;
		}
	}
	
	public boolean equals(Position position) {
		return position.getColonne() == colonne && position.getLigne() == ligne;
	}
}
