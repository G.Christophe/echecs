package fr.JeuEchec.christophegarcia;

public class Case {
	private Piece piece;
	
	public Case() {
		
	}
	
	public Case(Piece piece) {
		setPiece(piece);
	}
	
	public Piece getPiece() {
		return piece;
	}
	
	public void setPiece(Piece piece) {
		this.piece = piece;
	}
	
	public boolean estOccupee() {
		return piece != null;
	}
	
	public boolean estOccupee(String couleur) {
		if(estOccupee()) {
			return piece.getCouleur().equals(couleur);
		} else {
			return false;
		}
	}
}
