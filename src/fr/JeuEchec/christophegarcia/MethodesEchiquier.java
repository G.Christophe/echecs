package fr.JeuEchec.christophegarcia;

/**
 * Interface contenant les m�thodes que devra contenir un �chiquier. 
 * @author Christophe
 */
public interface MethodesEchiquier {
	
	public abstract void preparation();
	public abstract Case getCase(int colonne, int ligne);
	public abstract boolean cheminPossible(Piece piece, Deplacement deplacement);
}
