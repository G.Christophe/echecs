package fr.JeuEchec.christophegarcia;

public class Roi extends Piece {

	public Roi(String couleur) {
		super("roi", couleur);
	}

	public boolean estValide(Deplacement deplacement) {
		if (Math.abs(deplacement.getDeplacementX()) <= 1 && Math.abs(deplacement.getDeplacementY()) <= 1) { // Pas de
																											// d�placement
																											// plus loin
																											// que
																											// pr�vu.
			return true;
		} else if (couleur.equals("blanc")) {
			if (!Fenetre.petitRoqueBlancInterdit // Petit Roque blanc.
					&& deplacement.getDepart().getColonne() == 4 && deplacement.getDepart().getLigne() == 7
					&& deplacement.getDeplacementX() == 2 && deplacement.getDeplacementY() == 0
					&& !Fenetre.echiquier.getCase(5, 7).estOccupee() && !Fenetre.echiquier.getCase(6, 7).estOccupee()
					&& !Fenetre.echec("blanc", 4, 7) && !Fenetre.echec("blanc", 5, 7)
					&& !Fenetre.echec("blanc", 6, 7)) {
				Fenetre.petitRoqueBlancInterdit = true;
				Fenetre.grandRoqueBlancInterdit = true;
				return true;
			} else if (!Fenetre.grandRoqueBlancInterdit // Grand Roque blanc.
					&& deplacement.getDepart().getColonne() == 4 && deplacement.getDepart().getLigne() == 7
					&& deplacement.getDeplacementX() == -2 && deplacement.getDeplacementY() == 0
					&& !Fenetre.echiquier.getCase(1, 7).estOccupee() && !Fenetre.echiquier.getCase(2, 7).estOccupee()
					&& !Fenetre.echiquier.getCase(3, 7).estOccupee() && !Fenetre.echec("blanc", 4, 7)
					&& !Fenetre.echec("blanc", 3, 7) && !Fenetre.echec("blanc", 2, 7)) {
				Fenetre.petitRoqueBlancInterdit = true;
				Fenetre.grandRoqueBlancInterdit = true;
				return true;
			}
		} else {
			if (!Fenetre.petitRoqueNoirInterdit // Petit Roque noir.
					&& deplacement.getDepart().getColonne() == 4 && deplacement.getDepart().getLigne() == 0
					&& deplacement.getDeplacementX() == 2 && deplacement.getDeplacementY() == 0
					&& !Fenetre.echiquier.getCase(5, 0).estOccupee() && !Fenetre.echiquier.getCase(6, 0).estOccupee()
					&& !Fenetre.echec("noir", 4, 0) && !Fenetre.echec("noir", 5, 0) && !Fenetre.echec("noir", 6, 0)) {
				Fenetre.petitRoqueNoirInterdit = true;
				Fenetre.grandRoqueNoirInterdit = true;
				return true;
			} else if (!Fenetre.grandRoqueNoirInterdit // Grand Roque noir.
					&& deplacement.getDepart().getColonne() == 4 && deplacement.getDepart().getLigne() == 0
					&& deplacement.getDeplacementX() == -2 && deplacement.getDeplacementY() == 0
					&& !Fenetre.echiquier.getCase(1, 0).estOccupee() && !Fenetre.echiquier.getCase(2, 0).estOccupee()
					&& !Fenetre.echiquier.getCase(3, 0).estOccupee() && !Fenetre.echec("noir", 4, 0)
					&& !Fenetre.echec("noir", 3, 0) && !Fenetre.echec("noir", 2, 0)) {
				Fenetre.petitRoqueNoirInterdit = true;
				Fenetre.grandRoqueNoirInterdit = true;
				return true;
			}
		}
		return false;
	}
}
