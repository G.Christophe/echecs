package fr.JeuEchec.christophegarcia;

import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.ImageIcon;

public class Echiquier implements MethodesEchiquier {
	private Case[][] tabCases;

	public Echiquier() {
		tabCases = new Case[8][8];
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				tabCases[i][j] = new Case();
			}
		}
	}

	public void preparation() {
		tabCases[0][0].setPiece(new Tour("noir"));
		tabCases[1][0].setPiece(new Cavalier("noir"));
		tabCases[2][0].setPiece(new Fou("noir"));
		tabCases[3][0].setPiece(new Reine("noir"));
		tabCases[4][0].setPiece(new Roi("noir"));
		tabCases[5][0].setPiece(new Fou("noir"));
		tabCases[6][0].setPiece(new Cavalier("noir"));
		tabCases[7][0].setPiece(new Tour("noir"));
		for (int i = 0; i < 8; i++) {
			tabCases[i][1].setPiece(new Pion("noir"));
		}

		tabCases[0][7].setPiece(new Tour("blanc"));
		tabCases[1][7].setPiece(new Cavalier("blanc"));
		tabCases[2][7].setPiece(new Fou("blanc"));
		tabCases[3][7].setPiece(new Reine("blanc"));
		tabCases[4][7].setPiece(new Roi("blanc"));
		tabCases[5][7].setPiece(new Fou("blanc"));
		tabCases[6][7].setPiece(new Cavalier("blanc"));
		tabCases[7][7].setPiece(new Tour("blanc"));
		for (int i = 0; i < 8; i++) {
			tabCases[i][6].setPiece(new Pion("blanc"));
		}

		// Mise en place des icones de chaque pi�ce.
		Fenetre.tabLabels[0][0].setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("tour-noir.png")));
		Fenetre.tabLabels[1][0].setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("cavalier-noir.png")));
		Fenetre.tabLabels[2][0].setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("fou-noir.png")));
		Fenetre.tabLabels[3][0].setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("reine-noir.png")));
		Fenetre.tabLabels[4][0].setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("roi-noir.png")));
		Fenetre.tabLabels[5][0].setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("fou-noir.png")));
		Fenetre.tabLabels[6][0].setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("cavalier-noir.png")));
		Fenetre.tabLabels[7][0].setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("tour-noir.png")));
		for (int i = 0; i < 8; i++)
			Fenetre.tabLabels[i][1].setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("pion-noir.png")));
		Fenetre.tabLabels[0][7].setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("tour-blanc.png")));
		Fenetre.tabLabels[1][7].setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("cavalier-blanc.png")));
		Fenetre.tabLabels[2][7].setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("fou-blanc.png")));
		Fenetre.tabLabels[3][7].setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("reine-blanc.png")));
		Fenetre.tabLabels[4][7].setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("roi-blanc.png")));
		Fenetre.tabLabels[5][7].setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("fou-blanc.png")));
		Fenetre.tabLabels[6][7].setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("cavalier-blanc.png")));
		Fenetre.tabLabels[7][7].setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("tour-blanc.png")));
		for (int i = 0; i < 8; i++)
			Fenetre.tabLabels[i][6].setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("pion-blanc.png")));
	}

	public Case getCase(int colonne, int ligne) {
		return tabCases[colonne][ligne];
	}

	public boolean cheminPossible(Piece piece, Deplacement deplacement) {
		// Si la case d'arriv�e n'est pas occup�e par une piece de m�me couleur.
		if (!tabCases[deplacement.getArrivee().getColonne()][deplacement.getArrivee().getLigne()]
				.estOccupee(piece.getCouleur())) {
			// On v�rifie que la pi�ce n'est pas un Cavalier (car le cavalier saute par
			// dessus les autres pi�ces).
			if (!(piece instanceof Cavalier)) {
				// On v�rifie que la pi�ce n'est pas un Pion (le Pion ne peut avancer si une
				// pi�ce se trouve devant, mais peut aller d'une case en diagonale
				// si une pi�ce de couleur contraire s'y trouve.
				if (!(piece instanceof Pion)) {
					// On v�rifie que la pi�ce n'est pas un Roi (car le Roi ne peut pas se d�placer
					// � c�t� du Roi adverse).
					if (!(piece instanceof Roi)) {
						// On regarde si le d�placement fait plus qu'une seule case sur au moin un axe,
						// sinon pas besoin de v�rifier si une pi�ce g�ne le chemin.
						if (!(Math.abs(deplacement.getDeplacementX()) - Math.abs(deplacement.getDeplacementY()) <= 1
								&& Math.abs(deplacement.getDeplacementX())
										+ Math.abs(deplacement.getDeplacementY()) <= 1)) {
							// jumpX et jumpY sont les incr�mentations � utiliser pour parcourir chaque case
							// entre la case de d�part et celle d'arriv�e.
							int jumpX = deplacement.getDeplacementX() == 0 ? 0
									: (int) (deplacement.getArrivee().getColonne()
											- deplacement.getDepart().getColonne())
											/ Math.abs((int) deplacement.getArrivee().getColonne()
													- deplacement.getDepart().getColonne());
							int jumpY = deplacement.getDeplacementY() == 0 ? 0
									: (int) (deplacement.getArrivee().getLigne() - deplacement.getDepart().getLigne())
											/ Math.abs((int) deplacement.getArrivee().getLigne()
													- deplacement.getDepart().getLigne());

							// On v�rifie une � une si les cases s�parant le d�part et l'arriv�e ne sont pas
							// occup�es.
							for (int incX = (int) (deplacement.getDepart().getColonne()
									+ jumpX), incY = (int) (deplacement.getDepart().getLigne()
											+ jumpY); incX != deplacement.getArrivee().getColonne()
													|| incY != deplacement.getArrivee().getLigne(); incX = incX
															+ jumpX, incY = incY + jumpY) {
								if (tabCases[incX][incY].estOccupee())
									return false;
							}
							// Si aucune case n'est occup�e, c'est-�-dire, si le chemin � parcourir par la
							// pi�ce est enti�rement libre.
							return true;
						} else {
							// Le d�placement se fait d'une seule case, donc il ne peut y avoir de pi�ces
							// qui g�nent le passage,
							// et la case d'arriv�e a d�j� �t� v�rifi�e avant comme non-occup�e par une
							// pi�ce de m�me couleur.
							return true;
						}
					} else {
						// La pi�ce est un Roi. Il faut v�rifier si le Roi adverse ne se trouve pas sur
						// une case juxtapos�e de la case d'arriv�e.
						boolean presenceRoiAdverse = false;
						ArrayList<Position> casesRoiValides = new ArrayList<Position>();
						Position positionRoi = new Position(deplacement.getArrivee().getColonne(),
								deplacement.getArrivee().getLigne());
						// On r�cup�re toutes les cases valides pour le Roi.
						for (int i = 0; i < 8; i++) {
							for (int j = 0; j < 8; j++) {
								if (piece.estValide(new Deplacement(positionRoi, new Position(i, j)))) {
									casesRoiValides.add(new Position(i, j));
								}
							}
						}
						// On regarde si le Roi adverse se trouve parmis les cases valides
						Iterator<Position> it = casesRoiValides.iterator();
						Position pos = null;
						while (it.hasNext() && !presenceRoiAdverse) {
							pos = it.next();
							if (Fenetre.echiquier.getCase(pos.getColonne(), pos.getLigne()).getPiece() instanceof Roi
									&& Fenetre.echiquier.getCase(pos.getColonne(), pos.getLigne())
											.estOccupee(piece.getCouleur().equals("blanc") ? "noir" : "blanc")) {
								presenceRoiAdverse = true;
							}
						}
						it.remove();
						if (presenceRoiAdverse)
							return false;
						else
							return true;
					}
				} else {
					// La pi�ce est un Pion.
					// Si la Prise en Passant est possible, alors la case est forc�ment libre
					// puisque le Pion adverse vient de passer dessus.
					if (Fenetre.priseEnPassantPossible)
						return true;
					// Si le Pion avance de 1 ou 2 cases, et que la case d'arriv�e est libre et
					// qu'aucune pi�ce le g�ne.
					else if (deplacement.getDeplacementX() == 0
							&& !tabCases[deplacement.getArrivee().getColonne()][deplacement.getArrivee().getLigne()]
									.estOccupee()
							&& !tabCases[deplacement.getArrivee().getColonne()][piece.getCouleur().equals("blanc")
									? (deplacement.getDepart().getLigne() - 1)
									: (deplacement.getDepart().getLigne() + 1)].estOccupee()) {
						// Si le Pion avance de 1 case (la case d'arriv�e a d�j� �t� v�rifi�e si libre).
						if (deplacement.getDeplacementY() == -1 || deplacement.getDeplacementY() == 1) {
							return true;
						}
						// Si le Pion avance de 2 cases. (pion
						else if (deplacement.getDeplacementY() == -2) {
							if (!tabCases[deplacement.getDepart().getColonne()][deplacement.getDepart().getLigne() - 1]
									.estOccupee()) {
								return true;
							}
						} else if (deplacement.getDeplacementY() == 2) {
							if (!tabCases[deplacement.getDepart().getColonne()][deplacement.getDepart().getLigne() + 1]
									.estOccupee()) {
								return true;
							}
						}
					} else {
						// Si le Pion s'appr�te � capturer une pi�ce (d�placement de 1 case en
						// diagonale).
						if ((deplacement.getDeplacementY() == -1 || deplacement.getDeplacementY() == 1)
								&& (deplacement.getDeplacementX() == -1 || deplacement.getDeplacementX() == 1)) {
							// Si la case est occup�e (la case d'arriv�e a d�j� �t� v�rifi�e si elle �tait
							// occup�e par une pi�ce de m�me couleur).
							if (tabCases[deplacement.getArrivee().getColonne()][deplacement.getArrivee().getLigne()]
									.estOccupee()) {
								return true;
							}
						}
					}
					return false;
				}
			} else {
				// La pi�ce est un Cavalier. Le Cavalier ne traverse pas les cases mais les
				// saute, donc pas de probl�me de g�ne sur le passage.
				return true;
			}
		} else {
			// Si une pi�ce de m�me couleur occupe d�j� la case d'arriv�e, alors retourne
			// false.
			return false;
		}
	}
}
