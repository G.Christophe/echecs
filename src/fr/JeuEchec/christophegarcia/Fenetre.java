package fr.JeuEchec.christophegarcia;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import javax.sound.sampled.Clip;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

/**
 * <div style="background-color:black; border-radius:30px;"><font color="gold">
 * 
 * <h1 style="text-align:center; font-size:250%;">R�gles du jeu</h1>
 * 
 * <ul style="list-style-type:none;">
 * 
 * <li><h2>Le d�placement des pi�ces :</h2>
 * 
 * <ul style="list-style-type:none;">
 * <li>Les pions : <ul style="list-style-type:none;"><li><font color="white">Le pion avance uniquement devant lui d�une seule case. 
 * Lors de son premier d�placement, il peut exceptionnellement avancer d�une ou de deux cases. 
 * Pour prendre une pi�ce ennemie, il se d�place seulement en diagonale d�une seule case.</font></li></ul></li>
 * 
 * <li>La tour : <ul style="list-style-type:none;"><li><font color="white">La tour se d�place horizontalement ou verticalement 
 * d�autant de cases que vous le souhaitez.</font></li></ul></li>
 * 
 * <li>Le cavalier : <ul style="list-style-type:none;"><li><font color="white">Le cavalier saute par dessus les cases qui 
 * entourent sa position initiale (m�me s�il y a des pi�ces du m�me joueur) pour se poser sur une des cases qui lui est possible, 
 * c'est-�-dire, une des cases de couleur oppos�e, accessible en un d�placement vertical de 2 cases + un d�placement horizontal 
 * de 1 case, ou en un d�placement horizontal de 2 cases + un d�placement vertical de 1 case.</font></li></ul></li>
 * 
 * <li>Le fou : <ul style="list-style-type:none;"><li><font color="white">Le fou se d�place diagonalement d�autant de cases que 
 * vous le souhaitez.</font></li></ul></li>
 * 
 * <li>La reine : <ul style="list-style-type:none;"><li><font color="white">La reine se d�place diagonalement, horizontalement 
 * ou verticalement d�autant de cases que vous le souhaitez.</font></li></ul></li>
 * 
 * <li>Le roi : <ul style="list-style-type:none;"><li><font color="white">Le roi se d�place sur n�importe quelle case adjacente 
 * � sa position initiale.</font></li></ul></li>
 * </ul>
 * </li>
 * <li><h2>Les coups sp�ciaux :</h2>
 * <p>
 * <ul style="list-style-type:none;">
 * 
 * <li>Le roque : <ul style="list-style-type:none;"><li><font color="white">Le roque permet de d�placer le roi de 2 cases vers 
 * une des 2 tours et de positionner la tour en question derri�re le roi. Il ne peut plus �tre effectu� si le roi ou la tour en 
 * question ont effectu� un d�placement plus t�t dans la partie. Il est momentan�ment emp�ch� si le roi est �chec, ou si les cases 
 * de d�placement du roi sont contr�l�es, ou s�il y a des pi�ces entre le roi et la tour.</font></li></ul></li>
 * 
 * <li>La promotion : <ul style="list-style-type:none;"><li><font color="white">Lorsqu'un pion parvient jusqu�� la derni�re rang�e, 
 * alors il se transforme en reine, en tour, en fou ou en cavalier. La pi�ce promue est imm�diatement op�rationnelle.
 * </font></li></ul></li>
 * 
 * <li>La prise en passant : <ul style="list-style-type:none;"><li><font color="white">Lorsqu�un pion (ex: blanc) se d�place de 
 * deux cases (au cours de son premier d�placement), le joueur adverse peut le prendre "en passant" avec le pion (ex: noir) situ� 
 * juste � c�t� du pion blanc en se d�placant juste derri�re ce dernier. Ce coup n'est plus valable dans le futur si le joueur 
 * adverse d�cide d'effectuer un autre coup. Mais l'occasion peut �ventuellement se repr�senter.</font></li></ul></li>
 * </ul>
 * </p>
 * </li>
 * <li><h2>Le d�roulement du jeu :</h2>
 * 
 * <ul style="display:flex; list-style-type:none;">
 * 
 * <li>&nbsp;</li><ul style="list-style-type:none;"><li><font color="white">Au d�but d�une partie, ce sont toujours les blancs qui 
 * commencent. Lors d�un tour de jeu, le joueur peut effectuer soit un d�placement, soit une prise soit un coup sp�cial.
 * </font></li></ul></li>
 * </ul>
 * </li>
 * <li><h2>Le but du jeu :</h2>
 * <p>
 * <ul style="list-style-type:none;">
 * 
 * <li>�chec : <ul style="list-style-type:none;"><li><font color="white">Chaque fois que vous pouvez prendre le roi de votre 
 * adversaire � votre prochain coup, vous devez le pr�venir en annon�ant "�chec". Il doit alors tenter de sauver son roi lors de 
 * son tour.</font></li></ul></li>
 * 
 * <li>�chec et mat : <ul style="list-style-type:none;"><li><font color="white">Si vous avez annonc� "�chec" et qu'il ne parvient 
 * pas � sauver son roi, alors il est "�chec et mat". Vous remportez la partie.</font></li></ul></li>
 * 
 * <li>Pat : <ul style="list-style-type:none;"><li><font color="white">Une partie peut �galement se terminer sur un match nul. 
 * Si lors du tour d�un joueur, celui-ci ne peut d�placer aucune de ses pi�ces et que son roi n�est pas en �chec, on dit alors 
 * que la partie se termine par un � pat �.</font></li></ul></li>
 * </ul>
 * </p>
 * </li>
 * </ul>
 * 
 * <p style="text-align:center; font-size:150%;"><b>Good Luck. Have Fun.</b></p>
 * 
 * </font></div>
 * 
 * @author Christophe
 * @version 1.0
 */
public class Fenetre extends JFrame {
	//private Audio musiquePrincipale = new Audio();
	//private Audio sonBoutonsControles = new Audio();
	
	static Echiquier echiquier = new Echiquier(); // l'echiquier du jeu 
	static JLabel[][] tabLabels = new JLabel[8][8]; // tableau de labels 
	
	static JPanel panelControles = new JPanel(); // panel pour les pseudos, le champ d'informations, le boutton demarrage et le timer 
	private JPanel panelEchiquier = new JPanel(); // panel de l'echiquier 
	GridLayout gridLayout1 = new GridLayout(); // Layout pour panelEchiquier 
	
	private JButton boutonValiderNomJoueurs = new BoutonRond("Fenetre.getRessourceAsBytes(\"valider.png\")"); // Bouton pour valider certains param�tres 
	static JButton boutonLancer = new BoutonRond("Fenetre.getRessourceAsBytes(\"play.png\")"); // Bouton pour lancer la game 
	static JButton boutonPause = new BoutonRond("Fenetre.getRessourceAsBytes(\"pause.png\")"); // Bouton pour faire une pause sur le temps + boutons non cliquables 
	static JButton boutonRestart = new BoutonRond("Fenetre.getRessourceAsBytes(\"restart.png\")"); // Bouton pour recommencer la game (restart timer + remet les pieces) 
	private JPanel imageTourBlanc = new JPanel();
	private JPanel vertTourBlanc = new JPanel();
	static JLabel couleurTourBlanc = new JLabel();
	static JTextField champTemps = new JTextField(); // Barre affichage informations du jeu
	private JPanel vertTourNoir = new JPanel();
	JLabel couleurTourNoir = new JLabel();
	private JPanel imageTourNoir = new JPanel();
	private JButton boutonAbandonner = new BoutonRond("Fenetre.getRessourceAsBytes(\"abandonner.png\")"); // Boutons pour les acc�der aux options
	private JButton boutonChangerNom = new BoutonRond("Fenetre.getRessourceAsBytes(\"editNom.png\")"); // Boutons pour les acc�der aux options
	private JButton boutonOptions = new BoutonRond("Fenetre.getRessourceAsBytes(\"parametresO.png\")"); // Boutons pour les acc�der aux options
	static boolean surLeBouton = false;
	
	private JPanel panelGauche = new JPanel(); // panel pieces captur�es par le joueur blanc 
	private JPanel panelMortGauche = new JPanel();
	private JPanel panelNomJGauche = new JPanel();
	static JLabel nomJoueurGauche = new JLabel(); // nom du joueur gauche 
	private JPanel panelDroite = new JPanel(); // panel pieces captur�es par le joueur noir 
	private JPanel panelMortDroite = new JPanel();
	private JPanel panelNomJDroite = new JPanel();
	static JLabel nomJoueurDroite = new JLabel(); // Nom du joueur droit 
	
	// Pieces mang�es 
	private Font policeNbPiecesMortes = new Font("Arial", Font.BOLD, 50);
	private Color colorNbPiecesMortes = Color.white;
	private static JPanel pionMortGauche = new JPanel();		private JLabel pionMortGaucheLabel = new JLabel();
	private static JPanel pionMortDroite = new JPanel();		private JLabel pionMortDroiteLabel = new JLabel();
	private static JPanel tourMortGauche = new JPanel();		private JLabel tourMortGaucheLabel = new JLabel();
	private static JPanel tourMortDroite = new JPanel();		private JLabel tourMortDroiteLabel = new JLabel();
	private static JPanel cavalierMortGauche = new JPanel();	private JLabel cavalierMortGaucheLabel = new JLabel();
	private static JPanel cavalierMortDroite = new JPanel();	private JLabel cavalierMortDroiteLabel = new JLabel();
	private static JPanel fouMortGauche = new JPanel();			private JLabel fouMortGaucheLabel = new JLabel();
	private static JPanel fouMortDroite = new JPanel();			private JLabel fouMortDroiteLabel = new JLabel();
	private static JPanel reineMortGauche = new JPanel();		private JLabel reineMortGaucheLabel = new JLabel();
	private static JPanel reineMortDroite = new JPanel();		private JLabel reineMortDroiteLabel = new JLabel();
	
	static String nomJoueur1;	
	static String nomJoueur2;
	private Font nomJoueursFont = new Font("Arial", Font.BOLD, 50);
	
	static int heures;
	static int minutes;
	static int secondes;
	
	private int tempsSecondes = 0;
	static boolean preparationPiecesMortesFaite = false;
	static String titreAffichage; 
	static boolean affichageCoupSpecial = false;
	private boolean affichage1SEC = false;
	private boolean affichage2SEC = false;
	static boolean lancerEnable = true, pauseEnable = false, restartEnable = false;
	static String couleurControle = "blanc";
	private static ArrayList<Position> casesValides = new ArrayList<Position>();
	
	static boolean joueurGaucheAGagne = false;
	
	// Coups sp�ciaux. 
	static boolean priseEnPassantPossible = false;
	private int posXPiecePEP;
	private int posYPiecePEP;
	static boolean petitRoqueBlancInterdit = false;
	static boolean grandRoqueBlancInterdit = false;
	static boolean petitRoqueNoirInterdit = false;
	static boolean grandRoqueNoirInterdit = false;
	static boolean promotionPossible = false;
	static Thread thread = new Thread();
	static boolean aChoisiReine;
	static boolean aChoisiTour;
	static boolean aChoisiFou;
	static boolean aChoisiCavalier;
	static Piece piecePromotion = null;
	static ImageIcon piecePromotionIcon = null;
	static boolean fin = false;
	
	int nbPionMortGauche = 0;	int nbTourMortGauche = 0;	int nbCavalierMortGauche = 0;	int nbFouMortGauche = 0;	int nbReineMortGauche = 0;
	int nbPionMortDroite = 0;	int nbTourMortDroite = 0;	int nbCavalierMortDroite = 0;	int nbFouMortDroite = 0;	int nbReineMortDroite = 0;
	
	// Couleurs de l'�chiquier et des pi�ces mortes (orange, vert, marron, bleu, violet, gris, turquoise, rouge). 
	static String couleurPrincipale = "orange";
	
	static JLabel pionMortGaucheImage = new JLabel(new ImageIcon(Fenetre.getRessourceAsBytes("pionMort" + couleurPrincipale + ".png")));
	static JLabel pionMortDroiteImage = new JLabel(new ImageIcon(Fenetre.getRessourceAsBytes("pionMort" + couleurPrincipale + ".png")));
	static JLabel tourMortGaucheImage = new JLabel(new ImageIcon(Fenetre.getRessourceAsBytes("tourMort" + couleurPrincipale + ".png")));
	static JLabel tourMortDroiteImage = new JLabel(new ImageIcon(Fenetre.getRessourceAsBytes("tourMort" + couleurPrincipale + ".png")));
	static JLabel cavalierMortGaucheImage = new JLabel(new ImageIcon(Fenetre.getRessourceAsBytes("cavalierMort" + couleurPrincipale + ".png")));
	static JLabel cavalierMortDroiteImage = new JLabel(new ImageIcon(Fenetre.getRessourceAsBytes("cavalierMort" + couleurPrincipale + ".png")));
	static JLabel fouMortGaucheImage = new JLabel(new ImageIcon(Fenetre.getRessourceAsBytes("fouMort" + couleurPrincipale + ".png")));
	static JLabel fouMortDroiteImage = new JLabel(new ImageIcon(Fenetre.getRessourceAsBytes("fouMort" + couleurPrincipale + ".png")));
	static JLabel reineMortGaucheImage = new JLabel(new ImageIcon(Fenetre.getRessourceAsBytes("reineMort" + couleurPrincipale + ".png")));
	static JLabel reineMortDroiteImage = new JLabel(new ImageIcon(Fenetre.getRessourceAsBytes("reineMort" + couleurPrincipale + ".png")));
	
	// PROMOTION 
	private JPanel panelPrincipal = new JPanel();
	private JPanel panelTitre = new JPanel();
	private JPanel reinePanel = new JPanel();
	private JPanel tourPanel = new JPanel();
	private JPanel fouPanel = new JPanel();
	private JPanel cavalierPanel = new JPanel();
	
	private JLabel labelTitre = new JLabel("PROMOTION");
	private Font policeTitre = new Font("Arial", Font.BOLD, 50); // police titre 
	private Color colorBackground = new Color(100, 100, 100);
	
	static JLabel reineBouton = new JLabel();
	static JLabel tourBouton = new JLabel();
	static JLabel fouBouton = new JLabel();
	static JLabel cavalierBouton = new JLabel();
	static JButton boutonValider = new BoutonRond("Fenetre.getRessourceAsBytes(\"valider.png\")"); // Bouton pour valider le choix.
	
	private Clip clip;
	
	/**
	 * Main 
	 * @param args - arguments 
	 */
	public static void main (String args[]) {
		new NomJoueurs();
	}
	
	public static byte[] getRessourceAsBytes(String directory) {
		byte[] bytes = new byte[0];
		ByteArrayOutputStream reader = null;
		try {
			InputStream input = Fenetre.class.getResourceAsStream("/" + directory); 			
			reader = new ByteArrayOutputStream();
			int read;
			byte[] data = new byte[16384];
			while ((read = input.read(data, 0, data.length)) != -1) reader.write(data, 0, read);
			reader.flush();
			bytes = reader.toByteArray();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		try {
			reader.close();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return bytes;
	}
	
	/**
	 * Constructor 
	 */
	public Fenetre(String nomJoueur1, String nomJoueur2) {
		Fenetre.nomJoueur1 = nomJoueur1;
		Fenetre.nomJoueur2 = nomJoueur2;
		try {
			jbInit(); // Initialisation de la fenetre 
		} catch (Exception e) {
			e.printStackTrace();
		}
		echiquier.preparation();
		/*
		// Musique de fond. (Qui bugue... Elle revient en arri�re de 1 seconde toutes les 5 sec :/.)
    	try {
    		Clip clip = AudioSystem.getClip();
    		clip.open(AudioSystem.getAudioInputStream(new File("musiquePrincipale.wav")));
    		clip.start();
    		//Thread.sleep(clip.getMicrosecondLength() / 1000);
    	} catch(Exception e1) {
    		e1.getStackTrace();
    	}
    	*/
	}
	
	/**
	 * Initializes all 
	 * @throws Exception - exception 
	 */
	private void jbInit() throws Exception {
    	
		//icon parameters 
	    this.setIconImage(new ImageIcon(Fenetre.getRessourceAsBytes("icon.png")).getImage());
		
		//parametres fenetre 
	    this.setTitle("Jeu d'�chec");
	    //Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	    this.setBounds(0, 0, 1536, 824);
	    //this.setExtendedState(JFrame.MAXIMIZED_BOTH); 
	    this.setUndecorated(true);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setLocationRelativeTo(null);
	    this.setResizable(false);
	    this.getContentPane().setLayout(null);
	    
	    // Panel Contr�les. 
	    panelControles.setBounds(new Rectangle(0, 0, 1536, 64));
	    panelControles.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
	    panelControles.setLayout(null);
	    panelControles.add(boutonLancer, null);
	    panelControles.add(boutonPause, null);
	    panelControles.add(boutonRestart, null);
	    panelControles.add(imageTourBlanc, null);
	    panelControles.add(vertTourBlanc, null);
	    panelControles.add(champTemps, null);
	    panelControles.add(vertTourNoir, null);
	    panelControles.add(imageTourNoir, null);
	    panelControles.add(boutonAbandonner, null);
	    panelControles.add(boutonChangerNom, null);
	    panelControles.add(boutonOptions, null);
	    panelControles.setBackground(Color.darkGray);
	    panelControles.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.white));
	    	// Bouton Lancer. 
	    boutonLancer.setBounds(66, 7, 50, 50);
	    boutonLancer.setBorderPainted(false);
	    boutonLancer.setFocusPainted(false);
			// Bouton Pause. 
	    boutonPause.setBounds(166, 7, 50, 50);
	    boutonPause.setBorderPainted(false);
	    boutonPause.setFocusPainted(false);
			// Bouton Restart. 
	    boutonRestart.setBounds(266, 7, 50, 50);
	    boutonRestart.setBorderPainted(false);
	    boutonRestart.setFocusPainted(false);
			// Rond blanc (repr�sente Joueur blanc). 
	    imageTourBlanc.setBounds(383, 3, 58, 58);
	    imageTourBlanc.setBackground(Color.darkGray);
	    imageTourBlanc.add(new JLabel(new ImageIcon(Fenetre.getRessourceAsBytes("blanc.png"))));
	    	// Rond vert si c'est au tour du Joueur blanc de joueur, sinon rond gris. 
	    vertTourBlanc.setBounds(441, 17, 30, 30);
	    vertTourBlanc.setBackground(Color.darkGray);
	    couleurTourBlanc.setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("rondGris.png")));
	    vertTourBlanc.add(couleurTourBlanc);
	    	// Affichage du temps. 
	    champTemps.setBounds(471, 0, 585, 60);
	    champTemps.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.darkGray));
	    champTemps.setBackground(Color.darkGray);
	    champTemps.setHorizontalAlignment(JLabel.CENTER);
	    champTemps.setFont(nomJoueursFont);
	    champTemps.setForeground(Color.white);
	    champTemps.setEditable(false);
			// Rond vert si c'est au tour du Joueur noir de joueur, sinon rond gris. 
	    vertTourNoir.setBounds(1055, 17, 30, 30);
	    vertTourNoir.setBackground(Color.darkGray);
	    couleurTourNoir.setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("rondGris.png")));
	    vertTourNoir.add(couleurTourNoir);
			// Rond noir (repr�sente Joueur noir). 
	    imageTourNoir.setBounds(1093, 3, 58, 58);
	    imageTourNoir.setBackground(Color.darkGray);
	    imageTourNoir.add(new JLabel(new ImageIcon(Fenetre.getRessourceAsBytes("noir.png"))));
	    	// Bouton Abandonner. 
	    boutonAbandonner.setBounds(1210, 7, 50, 50);
	    boutonAbandonner.setBorderPainted(false);
	    boutonAbandonner.setFocusPainted(false);
	    	// Bouton ChangerNom. 
	    boutonChangerNom.setBounds(1310, 7, 50, 50);
	    boutonChangerNom.setBorderPainted(false);
	    boutonChangerNom.setFocusPainted(false);
	    	// Bouton Options. 
	    boutonOptions.setBounds(1410, 7, 50, 50);
	    boutonOptions.setBorderPainted(false);
	    boutonOptions.setFocusPainted(false);
	    
	    // Panel �chiquier 
	    panelEchiquier.setBounds(new Rectangle(383, 64, 760, 760));
	    panelEchiquier.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
	    gridLayout1.setColumns(8);
	    gridLayout1.setRows(8);
	    panelEchiquier.setLayout(gridLayout1);
	    
	    // Panels vides pour l'espace du bas des panels gauche et droite. 
	    JPanel panelVideGauche = new JPanel();
		panelVideGauche.setPreferredSize(new Dimension(383, 25));
		panelVideGauche.setBackground(Color.darkGray);
		JPanel panelVideDroite = new JPanel();
		panelVideDroite.setPreferredSize(new Dimension(383, 25));
		panelVideDroite.setBackground(Color.darkGray);
		
	    // Panel gauche. (Nom du Joueur blanc + pi�ces captur�es). 
	    panelGauche.setBounds(0, 64, 383, 760);
	    panelGauche.setLayout(new BoxLayout(panelGauche, BoxLayout.Y_AXIS));
	    panelGauche.setBackground(Color.darkGray);
	    panelGauche.add(panelNomJGauche);
		panelGauche.add(panelMortGauche);
		panelGauche.add(panelVideGauche);
		
		panelNomJGauche.setSize(383, 85);
		panelNomJGauche.setBackground(Color.darkGray);
		panelNomJGauche.add(nomJoueurGauche);
		nomJoueurGauche.setText(nomJoueur1);
	    nomJoueurGauche.setFont(nomJoueursFont);
	    nomJoueurGauche.setForeground(Color.white);
		panelMortGauche.setLayout(new GridLayout(5, 2, 65, 7));
		panelMortGauche.setBackground(Color.darkGray);
	    
		// Panel droit. (Nom du Joueur noir + pi�ces captur�es). 
		panelDroite.setBounds(1143, 64, 383, 760);
		panelDroite.setLayout(new BoxLayout(panelDroite, BoxLayout.Y_AXIS));
		panelDroite.setBackground(Color.darkGray);
		panelDroite.add(panelNomJDroite);
		panelDroite.add(panelMortDroite);
		panelDroite.add(panelVideDroite);
		
		panelNomJDroite.setSize(383, 85);
		panelNomJDroite.setBackground(Color.darkGray);
		panelNomJDroite.add(nomJoueurDroite);
		nomJoueurDroite.setText(nomJoueur2);
		nomJoueurDroite.setFont(nomJoueursFont);
		nomJoueurDroite.setForeground(Color.white);
		panelMortDroite.setLayout(new GridLayout(5, 2, 65, 7));
		panelMortDroite.setBackground(Color.darkGray);
		
	    // Panel principal du jeu. 
	    this.getContentPane().add(panelControles, null);
	    this.getContentPane().add(panelEchiquier, null);
	    this.getContentPane().add(panelGauche, null);
	    this.getContentPane().add(panelDroite, null);
	    
	    // Les �couteurs. 
	    EvenementsSouris gestEvent = new EvenementsSouris();
	    boutonValiderNomJoueurs.addMouseListener(gestEvent);
	    boutonLancer.addMouseListener(gestEvent);
	    boutonLancer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!preparationPiecesMortesFaite) {
					// Mise en place des r�les de chaque pi�ce. 
					echiquier.getCase(0, 0).setPiece(new Tour("noir"));
					echiquier.getCase(1, 0).setPiece(new Cavalier("noir"));
					echiquier.getCase(2, 0).setPiece(new Fou("noir"));
					echiquier.getCase(3, 0).setPiece(new Reine("noir"));
					echiquier.getCase(4, 0).setPiece(new Roi("noir"));
					echiquier.getCase(5, 0).setPiece(new Fou("noir"));
					echiquier.getCase(6, 0).setPiece(new Cavalier("noir"));
					echiquier.getCase(7, 0).setPiece(new Tour("noir"));
					for(int i = 0; i < 8; i++) echiquier.getCase(i, 1).setPiece(new Pion("noir"));
					echiquier.getCase(0, 7).setPiece(new Tour("blanc"));
					echiquier.getCase(1, 7).setPiece(new Cavalier("blanc"));
					echiquier.getCase(2, 7).setPiece(new Fou("blanc"));
					echiquier.getCase(3, 7).setPiece(new Reine("blanc"));
					echiquier.getCase(4, 7).setPiece(new Roi("blanc"));
					echiquier.getCase(5, 7).setPiece(new Fou("blanc"));
					echiquier.getCase(6, 7).setPiece(new Cavalier("blanc"));
					echiquier.getCase(7, 7).setPiece(new Tour("blanc"));
					for(int i = 0; i < 8; i++) echiquier.getCase(i, 6).setPiece(new Pion("blanc"));
				}
				
				piecesCapturees();
				
				boutonLancer.setEnabled(false);
				lancerEnable = false;
				boutonPause.setEnabled(true);
				pauseEnable = true;
				boutonRestart.setEnabled(true);
	    		restartEnable = true;
				
				Timer chrono = new Timer();
			    chrono.schedule(new TimerTask() {
			    	// Boucle tant que le bouton Pause est cliquable, c'est-�-dire, tant que la partie n'est pas en pause. 
			    	// Si le bouton Pause est cliqu�, alors le temps est en pause, il reprend lors du clic sur le bouton Lancer. 
					public void run() {
						if(pauseEnable) {
							int heures = (int) (tempsSecondes / 3600);
					        int minutes = (int) ((tempsSecondes % 3600) / 60);
					        int secondes = (int) (tempsSecondes % 60);
							
					        tempsSecondes++;
					        
					        if(!affichageCoupSpecial) {
					        	champTemps.setForeground(Color.white);
								champTemps.setText((heures < 10 ? "0" : "") + heures + " : " 
												 + (minutes < 10 ? "0" : "") + minutes + " : " 
												 + (secondes < 10 ? "0" : "") + secondes);
								Fenetre.heures = heures;
				        		Fenetre.minutes = minutes;
				        		Fenetre.secondes = secondes;
					        } else {
					        	if(titreAffichage.equals("ECHEC")) {
					        		champTemps.setText(titreAffichage);
					        		champTemps.setForeground(Color.red);
					        	} else if(titreAffichage.equals("PROMOTION")) {
					        		champTemps.setText(titreAffichage);
					        		champTemps.setForeground(new Color(82, 216, 253));
					        	} else if(titreAffichage.equals("PETIT ROQUE") || titreAffichage.equals("GRAND ROQUE")) {
					        		champTemps.setText(titreAffichage);
					        		champTemps.setForeground(new Color(255, 212, 0));
					        	} else if(titreAffichage.equals("PRISE EN PASSANT")) {
					        		champTemps.setText(titreAffichage);
					        		champTemps.setForeground(new Color(255, 201, 23));
					        	} else {
					        		Fenetre.heures = heures;
					        		Fenetre.minutes = minutes;
					        		Fenetre.secondes = secondes;
				        			cancel();
					        	}
					        	// Affichage du texte pendant 3 secondes. 
					        	if(affichage1SEC) {
					        		if(affichage2SEC) {
					        			affichageCoupSpecial = false;
					        			affichage1SEC = false;
					        			affichage2SEC = false;
					        		} else affichage2SEC = true;
					        	} else affichage1SEC = true;
					        }
						} else {
							cancel();
						}
					}
			    }, 0, 1000);
			}
		});
	    boutonPause.addMouseListener(gestEvent);
	    boutonPause.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		boutonLancer.setEnabled(true);
	    		lancerEnable = true;
	    		boutonPause.setEnabled(false);
				pauseEnable = false;
				boutonRestart.setEnabled(true);
	    		restartEnable = true;
	    	}
	    });
	    boutonRestart.setEnabled(false);
		restartEnable = false;
	    boutonRestart.addMouseListener(gestEvent);
	    boutonRestart.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		for (int ligne = 0; ligne < 8; ligne++) 
	    			for (int colonne = 0; colonne < 8; colonne++) {
	    				tabLabels[colonne][ligne].setIcon(null);
	    				echiquier.getCase(colonne, ligne).setPiece(null);
	    			}
	    		echiquier.preparation();
	    		piecesCapturees();
	    		
	    		heures = 0;
	    		minutes = 0;
	    		secondes = 0;
	    		tempsSecondes = 0;
	    		
	    		nbPionMortGauche = 0;
	    		nbTourMortGauche = 0;
	    		nbCavalierMortGauche = 0;
	    		nbFouMortGauche = 0;
	    		nbReineMortGauche = 0;
	    		nbPionMortDroite = 0;
	    		nbTourMortDroite = 0;
	    		nbCavalierMortDroite = 0;
	    		nbFouMortDroite = 0;
	    		nbReineMortDroite = 0;
	    		
	    		champTemps.setText(null);
	    		boutonLancer.setEnabled(true);
	    		lancerEnable = true;
	    		boutonPause.setEnabled(false);
	    		pauseEnable = false;
	    		boutonRestart.setEnabled(false);
	    		restartEnable = false;
	    		
	    		new EditNoms(nomJoueur1, nomJoueur2);
	    	}
	    });
	    boutonAbandonner.addMouseListener(gestEvent);
	    boutonAbandonner.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		boutonLancer.setEnabled(true);
	    		lancerEnable = true;
	    		boutonPause.setEnabled(false);
				pauseEnable = false;
				boutonRestart.setEnabled(true);
				restartEnable = true;
				
				new FinDePartie();
	    	}
	    });
	    boutonChangerNom.addMouseListener(gestEvent);
	    boutonChangerNom.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		boutonLancer.setEnabled(true);
	    		lancerEnable = true;
	    		boutonPause.setEnabled(false);
				pauseEnable = false;
				boutonRestart.setEnabled(true);
				restartEnable = true;
				
				new EditNoms(nomJoueur1, nomJoueur2);
	    	}
	    });
	    boutonOptions.addMouseListener(gestEvent);
	    boutonOptions.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		boutonLancer.setEnabled(true);
	    		lancerEnable = true;
	    		boutonPause.setEnabled(false);
	    		pauseEnable = false;
	    		boutonRestart.setEnabled(true);
				restartEnable = true;
				
				new Parametres();
	    	}
	    });
		
	    // Initialisation de l'�chiquier. 
	    int a = 1;
		for (byte ligne = 0; ligne < 8; ligne++) {
			a = a == 1 ? 0 : 1;
			for (byte colonne = 0; colonne < 8; colonne++) {
				tabLabels[colonne][ligne] = new JLabel();
				tabLabels[colonne][ligne].setOpaque(true);
				panelEchiquier.add(tabLabels[colonne][ligne]);
				tabLabels[colonne][ligne].setHorizontalAlignment(SwingConstants.CENTER);
				tabLabels[colonne][ligne].addMouseListener(gestEvent);
				if ((colonne + 1) % 2 == a) {
					// Couleur principale. 
					tabLabels[colonne][ligne].setBackground(new Color(209, 139, 70));
				} else {
					// Couleur secondaire. 
					tabLabels[colonne][ligne].setBackground(new Color(253, 206, 158));
				}
			}
		}
		
		aChoisiReine = false;
		aChoisiTour = false;
		aChoisiFou = false;
		aChoisiCavalier = false;
		
		this.setVisible(true);
	}
	
	public static void setCouleurEchiquier() {
		int a = 1;
		for (byte ligne = 0; ligne < 8; ligne++) {
			a = a == 1 ? 0 : 1;
			for (byte colonne = 0; colonne < 8; colonne++) {
				if ((colonne + 1) % 2 == a) {
					// Couleur principale. 
					if(couleurPrincipale == "orange") tabLabels[colonne][ligne].setBackground(new Color(209, 139, 70));
					if(couleurPrincipale == "vert") tabLabels[colonne][ligne].setBackground(new Color(118, 150, 86));
					if(couleurPrincipale == "marron") tabLabels[colonne][ligne].setBackground(new Color(118, 53, 40));
					if(couleurPrincipale == "bleu") tabLabels[colonne][ligne].setBackground(new Color(50, 100, 100));
					if(couleurPrincipale == "violet") tabLabels[colonne][ligne].setBackground(new Color(58, 11, 113));
					if(couleurPrincipale == "gris") tabLabels[colonne][ligne].setBackground(new Color(160, 160, 160));
					if(couleurPrincipale == "turquoise") tabLabels[colonne][ligne].setBackground(new Color(160, 172, 230));
					if(couleurPrincipale == "rouge") tabLabels[colonne][ligne].setBackground(new Color(191, 67, 67));
				} else {
					// Couleur secondaire. 
					if(couleurPrincipale == "orange") tabLabels[colonne][ligne].setBackground(new Color(253, 206, 158));
					if(couleurPrincipale == "vert") tabLabels[colonne][ligne].setBackground(new Color(238, 238, 210));
					if(couleurPrincipale == "marron") tabLabels[colonne][ligne].setBackground(new Color(233, 195, 117));
					if(couleurPrincipale == "bleu") tabLabels[colonne][ligne].setBackground(new Color(100, 200, 200));
					if(couleurPrincipale == "violet") tabLabels[colonne][ligne].setBackground(new Color(219, 26, 135));
					if(couleurPrincipale == "gris") tabLabels[colonne][ligne].setBackground(new Color(215, 215, 215));
					if(couleurPrincipale == "turquoise") tabLabels[colonne][ligne].setBackground(new Color(116, 235, 213));
					if(couleurPrincipale == "rouge") tabLabels[colonne][ligne].setBackground(new Color(255, 178, 178));
				}
			}
		}
		
		pionMortGaucheImage.setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("pionMort" + couleurPrincipale + ".png")));
		pionMortDroiteImage.setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("pionMort" + couleurPrincipale + ".png")));
		tourMortGaucheImage.setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("tourMort" + couleurPrincipale + ".png")));
		tourMortDroiteImage.setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("tourMort" + couleurPrincipale + ".png")));
		cavalierMortGaucheImage.setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("cavalierMort" + couleurPrincipale + ".png")));
		cavalierMortDroiteImage.setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("cavalierMort" + couleurPrincipale + ".png")));
		fouMortGaucheImage.setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("fouMort" + couleurPrincipale + ".png")));
		fouMortDroiteImage.setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("fouMort" + couleurPrincipale + ".png")));
		reineMortGaucheImage.setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("reineMort" + couleurPrincipale + ".png")));
		reineMortDroiteImage.setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("reineMort" + couleurPrincipale + ".png")));
	}
	
	public void piecesCapturees() {
		// Seulement lors de la pr�paration, c'est-�-dire, s'�xecute une seule fois : lors de premier clic sur le bouton Lancer. 
		if(!preparationPiecesMortesFaite) {
			// Tour au Joueur blanc. 
			couleurTourBlanc.setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("rondVert.png")));
			
			// Panel Pions captur�s. 
			pionMortGaucheLabel.setFont(policeNbPiecesMortes);
			pionMortGaucheLabel.setForeground(colorNbPiecesMortes);
			pionMortGaucheLabel.setHorizontalAlignment(SwingConstants.LEFT);
			pionMortGauche.setBackground(Color.darkGray);
			pionMortGauche.add(pionMortGaucheImage);
			panelMortGauche.add(pionMortGauche);
			panelMortGauche.add(pionMortGaucheLabel);
			
			pionMortDroiteLabel.setFont(policeNbPiecesMortes);
			pionMortDroiteLabel.setForeground(colorNbPiecesMortes);
			pionMortDroiteLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			pionMortDroite.setBackground(Color.darkGray);
			pionMortDroite.add(pionMortDroiteImage);
			panelMortDroite.add(pionMortDroiteLabel);
			panelMortDroite.add(pionMortDroite);
			
			// Panel Tours captur�es. 
			tourMortGaucheLabel.setFont(policeNbPiecesMortes);
			tourMortGaucheLabel.setForeground(colorNbPiecesMortes);
			tourMortGaucheLabel.setHorizontalAlignment(SwingConstants.LEFT);
			tourMortGauche.setBackground(Color.darkGray);
			tourMortGauche.add(tourMortGaucheImage);
			panelMortGauche.add(tourMortGauche);
			panelMortGauche.add(tourMortGaucheLabel);
			
			tourMortDroiteLabel.setFont(policeNbPiecesMortes);
			tourMortDroiteLabel.setForeground(colorNbPiecesMortes);
			tourMortDroiteLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			tourMortDroite.setBackground(Color.darkGray);
			tourMortDroite.add(tourMortDroiteImage);
			panelMortDroite.add(tourMortDroiteLabel);
			panelMortDroite.add(tourMortDroite);
			
			// Panel Cavaliers captur�s. 
			cavalierMortGaucheLabel.setFont(policeNbPiecesMortes);
			cavalierMortGaucheLabel.setForeground(colorNbPiecesMortes);
			cavalierMortGaucheLabel.setHorizontalAlignment(SwingConstants.LEFT);
			cavalierMortGauche.setBackground(Color.darkGray);
			cavalierMortGauche.add(cavalierMortGaucheImage);
			panelMortGauche.add(cavalierMortGauche);
			panelMortGauche.add(cavalierMortGaucheLabel);
			
			cavalierMortDroiteLabel.setFont(policeNbPiecesMortes);
			cavalierMortDroiteLabel.setForeground(colorNbPiecesMortes);
			cavalierMortDroiteLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			cavalierMortDroite.setBackground(Color.darkGray);
			cavalierMortDroite.add(cavalierMortDroiteImage);
			panelMortDroite.add(cavalierMortDroiteLabel);
			panelMortDroite.add(cavalierMortDroite);
			
			// Panel Fous captur�s. 
			fouMortGaucheLabel.setFont(policeNbPiecesMortes);
			fouMortGaucheLabel.setForeground(colorNbPiecesMortes);
			fouMortGaucheLabel.setHorizontalAlignment(SwingConstants.LEFT);
			fouMortGauche.setBackground(Color.darkGray);
			fouMortGauche.add(fouMortGaucheImage);
			panelMortGauche.add(fouMortGauche);
			panelMortGauche.add(fouMortGaucheLabel);
			
			fouMortDroiteLabel.setFont(policeNbPiecesMortes);
			fouMortDroiteLabel.setForeground(colorNbPiecesMortes);
			fouMortDroiteLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			fouMortDroite.setBackground(Color.darkGray);
			fouMortDroite.add(fouMortDroiteImage);
			panelMortDroite.add(fouMortDroiteLabel);
			panelMortDroite.add(fouMortDroite);
			
			// Panel Reines captur�es. 
			reineMortGaucheLabel.setFont(policeNbPiecesMortes);
			reineMortGaucheLabel.setForeground(colorNbPiecesMortes);
			reineMortGaucheLabel.setHorizontalAlignment(SwingConstants.LEFT);
			reineMortGauche.setBackground(Color.darkGray);
			reineMortGauche.add(reineMortGaucheImage);
			panelMortGauche.add(reineMortGauche);
			panelMortGauche.add(reineMortGaucheLabel);
			
			reineMortDroiteLabel.setFont(policeNbPiecesMortes);
			reineMortDroiteLabel.setForeground(colorNbPiecesMortes);
			reineMortDroiteLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			reineMortDroite.setBackground(Color.darkGray);
			reineMortDroite.add(reineMortDroiteImage);
			panelMortDroite.add(reineMortDroiteLabel);
			panelMortDroite.add(reineMortDroite);
		}
		
		preparationPiecesMortesFaite = true;
		
		// Nombre de Pions captur�s. 
		pionMortGaucheLabel.setText("" + nbPionMortGauche + "");
		pionMortDroiteLabel.setText("" + nbPionMortDroite + "");
		// Nombre de Tours captur�es. 
		tourMortGaucheLabel.setText("" + nbTourMortGauche + "");
		tourMortDroiteLabel.setText("" + nbTourMortDroite + "");
		// Nombre de Cavaliers captur�s. 
		cavalierMortGaucheLabel.setText("" + nbCavalierMortGauche + "");
		cavalierMortDroiteLabel.setText("" + nbCavalierMortDroite + "");
		// Nombre de Fous captur�s. 
		fouMortGaucheLabel.setText("" + nbFouMortGauche + "");
		fouMortDroiteLabel.setText("" + nbFouMortDroite + "");
		// Nombre de Reines captur�es. 
		reineMortGaucheLabel.setText("" + nbReineMortGauche + "");
		reineMortDroiteLabel.setText("" + nbReineMortDroite + "");
	}
	
	public static boolean echec(String couleurRoi, int colonneR, int ligneR) {
		boolean finDeLEchiquier = false;
		String couleurAdverse = couleurRoi.equals("blanc") ? "noir" : "blanc";
		int colonne = 0; 
		int ligne = 0;
		
		while(!finDeLEchiquier) {
			// Si la case est occup�e par une pi�ce adverse, alors on r�cup�re la pi�ce ainsi que sa position. 
			if(Fenetre.echiquier.getCase(colonne, ligne).estOccupee(couleurAdverse)) {
				Piece piece = Fenetre.echiquier.getCase(colonne, ligne).getPiece();
				Position positionPiece = new Position(colonne, ligne);
				
				// Si la pi�ce est un Cavalier. 
				if(piece instanceof Cavalier) {
					// Je stocke les positions valides de la pi�ce. 
					Deplacement deplacement = null;
					for(int i = 0; i < 8; i++) {
						for(int j = 0; j < 8; j++) {
							deplacement = new Deplacement(positionPiece, new Position(i, j));
							if(piece.estValide(deplacement)) {
								casesValides.add(new Position(i, j));
							}
						}
					}
					// Je regarde si le Roi se trouve sur une des positions valides de la pi�ce adverse. 
					Iterator<Position> it = casesValides.iterator();
					Position position = null;
					while(it.hasNext()) {
						position = it.next();
						if(position.getColonne() == colonneR && position.getLigne() == ligneR) {
							it.remove();
							return true;
						}
					}
				}
				// Si la pi�ce est un Pion. 
				else if(piece instanceof Pion) {
					// Pion blanc. 
					if(piece.getCouleur().equals("blanc")) {
						// On v�rifie de rester toujours dans les limites de l'�chiquier. 
						if(ligne - 1 >= 0) {
							if(colonne - 1 >= 0) {
								// On regarde si le Roi se trouve dans la diagonale avant gauche (de 1 case). 
								if(Fenetre.echiquier.getCase(colonne - 1, ligne - 1).estOccupee(couleurRoi) 
										&& Fenetre.echiquier.getCase(colonne - 1, ligne - 1).getPiece() instanceof Roi) {
									return true;
								}
							}
							if(colonne + 1 <= 7) {
								// On regarde si le Roi se trouve dans la diagonale avant droite (de 1 case). 
								if(Fenetre.echiquier.getCase(colonne + 1, ligne - 1).estOccupee(couleurRoi) 
										&& Fenetre.echiquier.getCase(colonne + 1, ligne - 1).getPiece() instanceof Roi) {
									return true;
								}
							}
						}
					} 
					// Pion noir. 
					else {
						// On v�rifie de rester toujours dans les limites de l'�chiquier. 
						if(ligne + 1 <= 7) {
							if(colonne - 1 >= 0) {
								// On regarde si le Roi se trouve dans la diagonale avant droite (de 1 case). 
								if(Fenetre.echiquier.getCase(colonne - 1, ligne + 1).estOccupee(couleurRoi) 
										&& Fenetre.echiquier.getCase(colonne - 1, ligne + 1).getPiece() instanceof Roi) {
									return true;
								}
							}
							if(colonne + 1 <= 7) {
								// On regarde si le Roi se trouve dans la diagonale avant gauche (de 1 case). 
								if(Fenetre.echiquier.getCase(colonne + 1, ligne + 1).estOccupee(couleurRoi) 
										&& Fenetre.echiquier.getCase(colonne + 1, ligne + 1).getPiece() instanceof Roi) {
									return true;
								}
							}
						}
					}
				}
				// Si la pi�ce est soit un Fou, soit une Tour, soit une Reine. 
				else if(piece instanceof Fou || piece instanceof Tour || piece instanceof Reine) {
					Deplacement deplacement = null;
					deplacement = new Deplacement(positionPiece, new Position(colonneR, ligneR));
					// Je regarde si le d�placement, dont l'arriv�e est la position du Roi, est possible et atteignable par la pi�ce. 
					if(piece.estValide(deplacement) && echiquier.cheminPossible(piece, deplacement)) {
						return true;
					}
				}
			}
			// Si on a regard� toutes les cases, alors le Roi n'est pas en �chec. 
			if(colonne == 7 && ligne == 7) {
				finDeLEchiquier = true;
			}
			// Sinon si on a regard� toute une colonne, on passe � la suivante. 
			else if(ligne == 7) {
				ligne = 0;
				colonne++;
			}
			// Sinon, on continue de regarder la colonne. 
			else {
				ligne++;
			}
			casesValides.clear();
		}
		return false;
	}
	
	private class EvenementsSouris extends MouseAdapter {
		
		//Promotion promotion = null;
		
		Piece laPiece = null;
		Piece laPieceCapturee = null;
		ImageIcon lIcone;
		ImageIcon lIconeCapturee;
		Position laPosition = null;
		byte colonneClic, ligneClic;
		byte colonneAncienClicBlanc, ligneAncienClicBlanc;
		byte colonneAncienClicRouge, ligneAncienClicRouge;
		int colonneRoi, ligneRoi;
		int colonneRoiAdverse, ligneRoiAdverse;
		boolean aEuEchec = false;
		boolean aEffectueLaPEP = false;
		
		// Clic de la souris. 
		public void mouseClicked(MouseEvent event) {
			if(event.getSource() instanceof JLabel) {
				if(!lancerEnable) {
					// Je d�termine le label sur lequel le joueur a cliqu�. 
					boolean caseTrouvee = false;
					byte i = 0, j = 0;
					while(!caseTrouvee && i < 8) {
						if(event.getSource() == tabLabels[i][j]) {
							colonneClic = i;
							ligneClic = j;
							caseTrouvee = true;
						} else {
							j++;
							if(j >= 8) {
								j = 0;
								i++;
							}
						}
					}
					
					// Je v�rifie si cette case est bien occup�e ou si la pi�ce qu'il tient dans la main n'est pas null. 
					if(echiquier.getCase(colonneClic, ligneClic).estOccupee() || laPiece != null) {
						tabLabels[colonneRoi][ligneRoi].setBorder(null);
						// Si la pi�ce qu'il va d�placer est null (le joueur va prendre une pi�ce). 
						if(laPiece == null) {
							// Si la pi�ce sur laquelle le joueur a cliqu� correspond � sa couleur. 
							if(echiquier.getCase(colonneClic, ligneClic).getPiece().getCouleur().equals(couleurControle)) {
								laPiece = echiquier.getCase(colonneClic, ligneClic).getPiece();
								lIcone = (ImageIcon) tabLabels[colonneClic][ligneClic].getIcon();
								
								laPosition = new Position(colonneClic, ligneClic);
								colonneAncienClicBlanc = colonneClic;
								ligneAncienClicBlanc = ligneClic;
								
								tabLabels[colonneClic][ligneClic].setBorder(BorderFactory.createLineBorder(Color.white, 5));
								tabLabels[colonneAncienClicRouge][ligneAncienClicRouge].setBorder(null);
								setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
							}
						}
						
						// Si la pi�ce qu'il va d�placer n'est pas null (le joueur va poser sa pi�ce). 
						else {
							// Cr�ation du d�placement en cours 
							Deplacement deplacement = new Deplacement(laPosition, new Position(colonneClic, ligneClic));
							
							// Si le joueur annule son d�placement. 
							if(deplacement.noDep()) {
								laPiece = null;
								lIcone = null;
								laPosition = null;
								tabLabels[colonneClic][ligneClic].setBorder(null);
								setCursor(Cursor.getDefaultCursor());
							}
							
							// Si le d�placement correspond aux d�placements r�alisables par la pi�ce  
							// et si le chemin pour atteindre la case d'arriv�e par la pi�ce est libre. 
							else if(laPiece.estValide(deplacement) && echiquier.cheminPossible(laPiece, deplacement)) {
								// On r�cup�re les coordonn�es du Roi du joueur. 
								for(int c = 0; c < 8; c++) {
									for(int l = 0; l < 8; l++) {
										if((Fenetre.echiquier.getCase(c, l).getPiece() instanceof Roi) && (Fenetre.echiquier.getCase(c, l).estOccupee(
												couleurControle))) {
											colonneRoi = c;
											ligneRoi = l;
										}
									}
								}
								// Si le Joueur effectue une Promotion. 
								if(!echec(couleurControle, colonneRoi, ligneRoi) 
										&& ((couleurControle.equals("blanc") && deplacement.getArrivee().getLigne() == 0)
											|| (couleurControle.equals("noir") && deplacement.getArrivee().getLigne() == 7))
										&& laPiece instanceof Pion) {
									laPosition = new Position(colonneClic, ligneClic);
									
									////////////////// FENETRE PROMOTION ///////////////////////
									
									reineBouton.setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("reine-" + couleurControle + ".png")));
									tourBouton.setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("tour-" + couleurControle + ".png")));
									fouBouton.setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("fou-" + couleurControle + ".png")));
									cavalierBouton.setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("cavalier-" + couleurControle + ".png")));
									if(echiquier.getCase(deplacement.getArrivee().getColonne(), deplacement.getArrivee().getLigne()).getPiece()
											!= null) {
										laPieceCapturee = echiquier.getCase(deplacement.getArrivee().getColonne(), deplacement.getArrivee().getLigne()).getPiece();
									}
									JFrame promotion = new JFrame();
									// Param�tres Icon. 
									promotion.setIconImage(new ImageIcon(Fenetre.getRessourceAsBytes("icon.png")).getImage());
									// Param�tres fen�tre. 
									promotion.setSize(610, 300);
									promotion.setTitle("Promotion");
									promotion.setUndecorated(true);
									promotion.setDefaultCloseOperation(NomJoueurs.DISPOSE_ON_CLOSE);
									promotion.setLocationRelativeTo(null);
									promotion.setResizable(false);
									promotion.setContentPane(panelPrincipal);
									// Les �couteurs. 
									EvenementsSouris gestEvent = new EvenementsSouris();
								    boutonValider.addMouseListener(gestEvent);
								    boutonValider.addActionListener(new ActionListener() {
										public void actionPerformed(ActionEvent e) {
											if(aChoisiReine || aChoisiTour || aChoisiFou || aChoisiCavalier) {
												if(aChoisiReine) {
													laPiece = new Reine(couleurControle.equals("blanc") ? "noir" : "blanc");
													lIcone = (ImageIcon) new ImageIcon(Fenetre.getRessourceAsBytes("reine-"+ (couleurControle.equals("blanc") ? "noir" : "blanc") +".png"));
												} else if(aChoisiTour) {
													laPiece = new Tour(couleurControle.equals("blanc") ? "noir" : "blanc");
													lIcone = (ImageIcon) new ImageIcon(Fenetre.getRessourceAsBytes("tour-"+ (couleurControle.equals("blanc") ? "noir" : "blanc") +".png"));
												} else if(aChoisiFou) {
													laPiece = new Fou(couleurControle.equals("blanc") ? "noir" : "blanc");
													lIcone = (ImageIcon) new ImageIcon(Fenetre.getRessourceAsBytes("fou-"+ (couleurControle.equals("blanc") ? "noir" : "blanc") +".png"));
												} else if(aChoisiCavalier) {
													laPiece = new Cavalier(couleurControle.equals("blanc") ? "noir" : "blanc");
													lIcone = (ImageIcon) new ImageIcon(Fenetre.getRessourceAsBytes("cavalier-"+ (couleurControle.equals("blanc") ? "noir" : "blanc") +".png"));
												}
												// On enl�ve donc le pion qui va se faire promouvoir. 
												echiquier.getCase(deplacement.getDepart().getColonne(), deplacement.getDepart().getLigne()).setPiece(null);
												tabLabels[colonneAncienClicBlanc][ligneAncienClicBlanc].setIcon(null);
												tabLabels[colonneAncienClicBlanc][ligneAncienClicBlanc].setBorder(null);
												tabLabels[colonneRoi][ligneRoi].setBorder(null);
												// On enl�ve la pi�ce �ventuellement mang�e par le pion.
												
												// Affichage de la nouvelle pi�ce. 
												tabLabels[colonneClic][ligneClic].setIcon(lIcone);
												echiquier.getCase(colonneClic, ligneClic).setPiece(laPiece);
												// Affichage du coup sp�cial en haut de la fen�tre. 
												affichageCoupSpecial = true;
												titreAffichage = "PROMOTION";
												
												laPiece = null;
												lIcone = null;
												laPosition = null;
												reineBouton.setIcon(null);
												tourBouton.setIcon(null);
												fouBouton.setIcon(null);
												cavalierBouton.setIcon(null);
												aChoisiReine = false;
												aChoisiTour = false;
												aChoisiFou = false;
												aChoisiCavalier = false;
												tabLabels[colonneClic][ligneClic].setBorder(null);
												setCursor(Cursor.getDefaultCursor());
												
												promotion.dispose();
											}
										}
									});
								    reinePanel.addMouseListener(gestEvent);
								    tourPanel.addMouseListener(gestEvent);
								    fouPanel.addMouseListener(gestEvent);
								    cavalierPanel.addMouseListener(gestEvent);
								    // Panel Principal. 
								 	panelPrincipal.setBackground(colorBackground);
								 	panelPrincipal.setLayout(null);
								 	panelPrincipal.add(panelTitre, null);
								 	panelPrincipal.add(reinePanel, null);
								 	panelPrincipal.add(tourPanel, null);
								 	panelPrincipal.add(fouPanel, null);
								 	panelPrincipal.add(cavalierPanel, null);
								 	panelPrincipal.add(boutonValider, null);
								 	// Panel Titre "PROMOTION". 
								 	panelTitre.setBackground(colorBackground);
								 	panelTitre.setBounds(0, 0, 610, 70);
								 	panelTitre.add(labelTitre);
								 	labelTitre.setForeground(Color.white);
								 	labelTitre.setFont(policeTitre);
									// Reine. 
								 	reinePanel.setBounds(5, 70, 150, 150);
								 	reinePanel.setLayout(null);
								 	reinePanel.setBackground(colorBackground);
								 	reinePanel.add(reineBouton);
								 	reineBouton.setHorizontalAlignment(SwingConstants.CENTER);
								    reineBouton.setBounds(0, 0, 150, 150);
								    // Tour. 
								 	tourPanel.setBounds(155, 70, 150, 150);
								 	tourPanel.setLayout(null);
								 	tourPanel.setBackground(colorBackground);
								 	tourPanel.add(tourBouton);
								 	tourBouton.setHorizontalAlignment(SwingConstants.CENTER);
								    tourBouton.setBounds(0, 0, 150, 150);
								    // Fou. 
								    fouPanel.setBounds(305, 70, 150, 150);
								    fouPanel.setLayout(null);
								    fouPanel.setBackground(colorBackground);
								    fouPanel.add(fouBouton);
								    fouBouton.setHorizontalAlignment(SwingConstants.CENTER);
								    fouBouton.setBounds(0, 0, 150, 150);
								    // Cavalier. 
								    cavalierPanel.setBounds(455, 70, 150, 150);
								    cavalierPanel.setLayout(null);
								    cavalierPanel.setBackground(colorBackground);
								    cavalierPanel.add(cavalierBouton);
								    cavalierBouton.setHorizontalAlignment(SwingConstants.CENTER);
								    cavalierBouton.setBounds(0, 0, 150, 150);
								    // Bouton Valider. 
								 	boutonValider.setBounds(215, 230, 180, 60);
								 	boutonValider.setBorderPainted(false);
								 	boutonValider.setFocusPainted(false);
								 	promotion.setVisible(true);
								 	
								 	////////////////// FENETRE PROMOTION ///////////////////////
								 	
								}
								// Si le Joueur r�alise le Roque. 
								else if(laPiece instanceof Roi && Math.abs(deplacement.getDeplacementX()) > 1) {
									if(couleurControle.equals("blanc")) {
										// Petit Roque blanc. 
										if(deplacement.getDeplacementX() == 2) {
											// lIconeCapturee et laPieceCapturee sont utilis�es pour la Tour concern�e par le Roque 
											// (temporaire juste pour ce d�placement).
											lIconeCapturee = (ImageIcon) tabLabels[7][7].getIcon();
											laPieceCapturee = echiquier.getCase(7, 7).getPiece();
											// On enl�ve le Roi de la case de d�part. 
											echiquier.getCase(laPosition.getColonne(), laPosition.getLigne()).setPiece(null);
											tabLabels[colonneAncienClicBlanc][ligneAncienClicBlanc].setIcon(null);
											tabLabels[colonneAncienClicBlanc][ligneAncienClicBlanc].setBorder(null);
											tabLabels[colonneRoi][ligneRoi].setBorder(null);
											// Ainsi que la Tour concern�e par le Roque. 
											echiquier.getCase(7, 7).setPiece(null);
											tabLabels[7][7].setIcon(null);
											// On place le Roi � la case d'arriv�e. 
											tabLabels[colonneClic][ligneClic].setIcon(lIcone);
											echiquier.getCase(colonneClic, ligneClic).setPiece(laPiece);
											// Ainsi que la Tour concern�e par le Roque. 
											tabLabels[5][7].setIcon(lIconeCapturee);
											echiquier.getCase(5, 7).setPiece(laPieceCapturee);
											setCursor(Cursor.getDefaultCursor());
											// Affichage du coup sp�cial en haut de la fen�tre. 
											affichageCoupSpecial = true;
											titreAffichage = "PETIT ROQUE";
										}
										// Grand Roque blanc. 
										else if(deplacement.getDeplacementX() == -2) {
											// lIconeCapturee et laPieceCapturee sont utilis�es pour la Tour concern�e par le Roque 
											// (temporaire juste pour ce d�placement).
											lIconeCapturee = (ImageIcon) tabLabels[0][7].getIcon();
											laPieceCapturee = echiquier.getCase(0, 7).getPiece();
											// On enl�ve le Roi de la case de d�part. 
											echiquier.getCase(laPosition.getColonne(), laPosition.getLigne()).setPiece(null);
											tabLabels[colonneAncienClicBlanc][ligneAncienClicBlanc].setIcon(null);
											tabLabels[colonneAncienClicBlanc][ligneAncienClicBlanc].setBorder(null);
											tabLabels[colonneRoi][ligneRoi].setBorder(null);
											// Ainsi que la Tour concern�e par le Roque. 
											echiquier.getCase(0, 7).setPiece(null);
											tabLabels[0][7].setIcon(null);
											// On place le Roi � la case d'arriv�e. 
											tabLabels[colonneClic][ligneClic].setIcon(lIcone);
											echiquier.getCase(colonneClic, ligneClic).setPiece(laPiece);
											// Ainsi que la Tour concern�e par le Roque. 
											tabLabels[3][7].setIcon(lIconeCapturee);
											echiquier.getCase(3, 7).setPiece(laPieceCapturee);
											setCursor(Cursor.getDefaultCursor());
											// Affichage du coup sp�cial en haut de la fen�tre. 
											affichageCoupSpecial = true;
											titreAffichage = "GRAND ROQUE";
										}
										
										// Si son Roi se trouve en position d'�chec, alors on emp�che le d�placement, sinon on continue la partie. 
										if(echec(couleurControle, colonneRoi, ligneRoi)) {
											aEuEchec = true;
											
											tabLabels[colonneRoi][ligneRoi].setBorder(BorderFactory.createLineBorder(Color.yellow, 5));
											
											echiquier.getCase(laPosition.getColonne(), laPosition.getLigne()).setPiece(laPiece);
											tabLabels[colonneAncienClicBlanc][ligneAncienClicBlanc].setIcon(lIcone);
											
											// Si une pi�ce avait �t� mang�e, alors on la remet � sa place, sinon on remet rien. 
											if(titreAffichage.equals("PETIT ROQUE")) {
												echiquier.getCase(7, 7).setPiece(laPieceCapturee);
												tabLabels[7][7].setIcon(lIconeCapturee);
											} else {
												echiquier.getCase(0, 7).setPiece(laPieceCapturee);
												tabLabels[0][7].setIcon(lIconeCapturee);
											}
										} else {
											aEuEchec = false;
										}
										
										laPieceCapturee = null;
										lIconeCapturee = null;
									} else {
										// Petit Roque noir. 
										if(deplacement.getDeplacementX() == 2) {
											// lIconeCapturee et laPieceCapturee sont utilis�es pour la Tour concern�e par le Roque 
											// (temporaire juste pour ce d�placement).
											lIconeCapturee = (ImageIcon) tabLabels[7][0].getIcon();
											laPieceCapturee = echiquier.getCase(7, 0).getPiece();
											// On enl�ve le Roi de la case de d�part. 
											echiquier.getCase(laPosition.getColonne(), laPosition.getLigne()).setPiece(null);
											tabLabels[colonneAncienClicBlanc][ligneAncienClicBlanc].setIcon(null);
											tabLabels[colonneAncienClicBlanc][ligneAncienClicBlanc].setBorder(null);
											tabLabels[colonneRoi][ligneRoi].setBorder(null);
											// Ainsi que la Tour concern�e par le Roque. 
											echiquier.getCase(7, 0).setPiece(null);
											tabLabels[7][0].setIcon(null);
											// On place le Roi � la case d'arriv�e. 
											tabLabels[colonneClic][ligneClic].setIcon(lIcone);
											echiquier.getCase(colonneClic, ligneClic).setPiece(laPiece);
											// Ainsi que la Tour concern�e par le Roque. 
											tabLabels[5][0].setIcon(lIconeCapturee);
											echiquier.getCase(5, 0).setPiece(laPieceCapturee);
											setCursor(Cursor.getDefaultCursor());
											// Affichage du coup sp�cial en haut de la fen�tre. 
											affichageCoupSpecial = true;
											titreAffichage = "PETIT ROQUE";
										}
										// Grand Roque noir. 
										else if(deplacement.getDeplacementX() == -2) {
											// lIconeCapturee et laPieceCapturee sont utilis�es pour la Tour concern�e par le Roque 
											// (temporaire juste pour ce d�placement).
											lIconeCapturee = (ImageIcon) tabLabels[0][0].getIcon();
											laPieceCapturee = echiquier.getCase(0, 0).getPiece();
											// On enl�ve le Roi de la case de d�part. 
											echiquier.getCase(laPosition.getColonne(), laPosition.getLigne()).setPiece(null);
											tabLabels[colonneAncienClicBlanc][ligneAncienClicBlanc].setIcon(null);
											tabLabels[colonneAncienClicBlanc][ligneAncienClicBlanc].setBorder(null);
											tabLabels[colonneRoi][ligneRoi].setBorder(null);
											// Ainsi que la Tour concern�e par le Roque. 
											echiquier.getCase(0, 0).setPiece(null);
											tabLabels[0][0].setIcon(null);
											// On place le Roi � la case d'arriv�e. 
											tabLabels[colonneClic][ligneClic].setIcon(lIcone);
											echiquier.getCase(colonneClic, ligneClic).setPiece(laPiece);
											// Ainsi que la Tour concern�e par le Roque. 
											tabLabels[3][0].setIcon(lIconeCapturee);
											echiquier.getCase(3, 0).setPiece(laPieceCapturee);
											setCursor(Cursor.getDefaultCursor());
											// Affichage du coup sp�cial en haut de la fen�tre. 
											affichageCoupSpecial = true;
											titreAffichage = "GRAND ROQUE";
										}
										// Si son Roi se trouve en position d'�chec, alors on emp�che le d�placement, sinon on continue la partie. 
										if(echec(couleurControle, colonneRoi, ligneRoi)) {
											aEuEchec = true;
											
											tabLabels[colonneRoi][ligneRoi].setBorder(BorderFactory.createLineBorder(Color.yellow, 5));
											
											echiquier.getCase(laPosition.getColonne(), laPosition.getLigne()).setPiece(laPiece);
											tabLabels[colonneAncienClicBlanc][ligneAncienClicBlanc].setIcon(lIcone);
											
											// Si une pi�ce avait �t� mang�e, alors on la remet � sa place, sinon on remet rien. 
											if(titreAffichage.equals("PETIT ROQUE")) {
												echiquier.getCase(7, 0).setPiece(laPieceCapturee);
												tabLabels[7][0].setIcon(lIconeCapturee);
											} else {
												echiquier.getCase(0, 0).setPiece(laPieceCapturee);
												tabLabels[0][0].setIcon(lIconeCapturee);
											}
										} else {
											aEuEchec = false;
										}
										
										laPieceCapturee = null;
										lIconeCapturee = null;
									}
								} 
								// Si le Joueur ne r�alise ni la Prise en Passant ni le Roque ni la Promotion. 
								else {
									aEffectueLaPEP = false;
									if(laPiece instanceof Pion) {
										// On regarde si le Joueur adverse pourra effectuer la Prise en Passant lors du prochaine tour. 
										if(deplacement.getDeplacementY() == -2 || deplacement.getDeplacementY() == 2) {
											priseEnPassantPossible = true;
											posXPiecePEP = deplacement.getArrivee().getColonne();
											posYPiecePEP = deplacement.getArrivee().getLigne();
										}
										// On regarde si le Joueur peut effectuer la Prise en Passant. 
										else if(priseEnPassantPossible && laPiece instanceof Pion) {
											priseEnPassantPossible = false;
											// Si le Pion qui a �t� d�plac� juste avant se trouve juste � c�t� du Pion pris en main. 
											// Et si le Joueur d�place son Pion comme s'il capturait une pi�ce, sur la m�me colonne que le Pion pr�c�dent. 
											if(deplacement.getDepart().getLigne() == posYPiecePEP 
												&& (deplacement.getDepart().getColonne() == posXPiecePEP - 1 
													|| deplacement.getDepart().getColonne() == posXPiecePEP + 1)
												&& ((couleurControle.equals("blanc") && deplacement.getArrivee().getColonne() == posXPiecePEP) 
													|| (couleurControle.equals("noir") && deplacement.getArrivee().getColonne() == posXPiecePEP)) ) {
												// lIconeCapturee et laPieceCapturee sont donc utilis�es pour le Pion pris en passant. 
												lIconeCapturee = (ImageIcon) tabLabels[posXPiecePEP][posYPiecePEP].getIcon();
												laPieceCapturee = echiquier.getCase(posXPiecePEP, posYPiecePEP).getPiece();
												// On enl�ve le Pion de la case de d�part. 
												echiquier.getCase(laPosition.getColonne(), laPosition.getLigne()).setPiece(null);
												tabLabels[colonneAncienClicBlanc][ligneAncienClicBlanc].setIcon(null);
												tabLabels[colonneAncienClicBlanc][ligneAncienClicBlanc].setBorder(null);
												tabLabels[colonneRoi][ligneRoi].setBorder(null);
												// Ainsi que le Pion pris en passant. 
												echiquier.getCase(posXPiecePEP, posYPiecePEP).setPiece(null);
												tabLabels[posXPiecePEP][posYPiecePEP].setIcon(null);
												// On place le Pion � la case d'arriv�e. 
												tabLabels[colonneClic][ligneClic].setIcon(lIcone);
												echiquier.getCase(colonneClic, ligneClic).setPiece(laPiece);
												setCursor(Cursor.getDefaultCursor());
												// Affichage du coup sp�cial en haut de la fen�tre. 
												affichageCoupSpecial = true;
												titreAffichage = "PRISE EN PASSANT";
												
												// On r�cup�re les coordonn�es du Roi du joueur. 
												for(int c = 0; c < 8; c++) {
													for(int l = 0; l < 8; l++) {
														if((Fenetre.echiquier.getCase(c, l).getPiece() instanceof Roi) && (Fenetre.echiquier.getCase(c, l).estOccupee(
																couleurControle))) {
															colonneRoi = c;
															ligneRoi = l;
														}
													}
												}
												// Si son Roi se trouve en position d'�chec, alors on emp�che le d�placement, sinon on continue la partie. 
												if(echec(couleurControle, colonneRoi, ligneRoi)) {
													aEuEchec = true;
													
													tabLabels[colonneRoi][ligneRoi].setBorder(BorderFactory.createLineBorder(Color.yellow, 5));
													
													echiquier.getCase(laPosition.getColonne(), laPosition.getLigne()).setPiece(laPiece);
													tabLabels[colonneAncienClicBlanc][ligneAncienClicBlanc].setIcon(lIcone);
													echiquier.getCase(posXPiecePEP, posYPiecePEP).setPiece(laPieceCapturee);
													tabLabels[posXPiecePEP][posYPiecePEP].setIcon(lIconeCapturee);
												} else {
													aEuEchec = false;
												}
												aEffectueLaPEP = true;
											}
										} else {
											priseEnPassantPossible = false;
											posXPiecePEP = -1;
											posYPiecePEP = -1;
										}
									}
									
									// D�placement de la pi�ce, et on r�cup�re la pi�ce captur�e, pour la remettre � sa place si le d�placement est impossible. 
									// Uniquement si la Prise en Passant n'a pas �t� faite juste � l'instant. 
									if(!aEffectueLaPEP) {
										if(echiquier.getCase(colonneClic, ligneClic).estOccupee()) {
											laPieceCapturee = echiquier.getCase(colonneClic, ligneClic).getPiece();
											lIconeCapturee = (ImageIcon) tabLabels[colonneClic][ligneClic].getIcon();
										}
										echiquier.getCase(laPosition.getColonne(), laPosition.getLigne()).setPiece(null);
										tabLabels[colonneAncienClicBlanc][ligneAncienClicBlanc].setIcon(null);
										tabLabels[colonneAncienClicBlanc][ligneAncienClicBlanc].setBorder(null);
										tabLabels[colonneRoi][ligneRoi].setBorder(null);
										
										tabLabels[colonneClic][ligneClic].setIcon(lIcone);
										echiquier.getCase(colonneClic, ligneClic).setPiece(laPiece);
										
										setCursor(Cursor.getDefaultCursor());
									}
									
									// On r�cup�re les coordonn�es du Roi du joueur. 
									for(int c = 0; c < 8; c++) {
										for(int l = 0; l < 8; l++) {
											if((Fenetre.echiquier.getCase(c, l).getPiece() instanceof Roi) && (Fenetre.echiquier.getCase(c, l).estOccupee(
													couleurControle))) {
												colonneRoi = c;
												ligneRoi = l;
											}
										}
									}
									
									// Si son Roi se trouve en position d'�chec, alors on emp�che le d�placement, sinon on continue la partie. 
									if(!aEffectueLaPEP && echec(couleurControle, colonneRoi, ligneRoi)) {
										aEuEchec = true;
										
										tabLabels[colonneRoi][ligneRoi].setBorder(BorderFactory.createLineBorder(Color.yellow, 5));
										
										echiquier.getCase(laPosition.getColonne(), laPosition.getLigne()).setPiece(laPiece);
										tabLabels[colonneAncienClicBlanc][ligneAncienClicBlanc].setIcon(lIcone);
										
										// Si une pi�ce avait �t� mang�e, alors on la remet � sa place, sinon on remet rien. 
										if(laPieceCapturee != null) {
											echiquier.getCase(colonneClic, ligneClic).setPiece(laPieceCapturee);
											tabLabels[colonneClic][ligneClic].setIcon(lIconeCapturee);
										} else {
											echiquier.getCase(colonneClic, ligneClic).setPiece(null);
											tabLabels[colonneClic][ligneClic].setIcon(null);
										}
									} else {
										aEuEchec = false;
									}
								}
								
								// S'il n'y a pas eu d'�chec.  
								if(!aEuEchec) {
									// Si la case est occup�e (donc forc�ment : capture d'une pi�ce). 
									if(laPieceCapturee != null) {
										if(couleurControle.equals("blanc")) {
											if(laPieceCapturee instanceof Pion) nbPionMortGauche++;
											else if(laPieceCapturee instanceof Tour) nbTourMortGauche++;
											else if(laPieceCapturee instanceof Cavalier) nbCavalierMortGauche++;
											else if(laPieceCapturee instanceof Fou) nbFouMortGauche++;
											else if(laPieceCapturee instanceof Reine) nbReineMortGauche++;
										} else {
											if(laPieceCapturee instanceof Pion) nbPionMortDroite++;
											else if(laPieceCapturee instanceof Tour) nbTourMortDroite++;
											else if(laPieceCapturee instanceof Cavalier) nbCavalierMortDroite++;
											else if(laPieceCapturee instanceof Fou) nbFouMortDroite++;
											else if(laPieceCapturee instanceof Reine) nbReineMortDroite++;
										}
										piecesCapturees();
									}
									
									String couleurAdverse = couleurControle.equals("blanc") ? "noir" : "blanc";
									// On r�cup�re les coordonn�es du Roi adverse pour v�rifier par la suite s'il se trouve en position d'�chec. 
									for(int c = 0; c < 8; c++) {
										for(int l = 0; l < 8; l++) {
											if(Fenetre.echiquier.getCase(c, l).getPiece() instanceof Roi && Fenetre.echiquier.getCase(c, l).estOccupee(
													couleurAdverse)) {
												colonneRoiAdverse = c;
												ligneRoiAdverse = l;
											}
										}
									}
									
									// Si le Roi est en �chec on affiche le texte "ECHEC" en haut de la fen�tre, sinon on affiche rien.  
									if(echec(couleurAdverse, colonneRoiAdverse, ligneRoiAdverse)) {
										affichageCoupSpecial = true;
										titreAffichage = "ECHEC";
									}
									
									// Si le Roi a �t� d�plac�, alors le joueur ne pourra jamais r�aliser le Roque. 
									// Si la Tour de gauche a �t� d�plac�e, alors le joueur ne pourra jamais r�aliser le grand Roque. 
									// Si la Tour de droite a �t� d�plac�e, alors le joueur ne pourra jamais r�aliser le petit Roque. 
									if(laPiece instanceof Roi) {
										if(couleurControle.equals("blanc")) {
											petitRoqueBlancInterdit = true;
											grandRoqueBlancInterdit = true;
										} else {
											petitRoqueNoirInterdit = true;
											grandRoqueNoirInterdit = true;
										}
									} else if(laPiece instanceof Tour) {
										if(colonneAncienClicBlanc == 0 && ligneAncienClicBlanc == 7) grandRoqueBlancInterdit = true;
										if(colonneAncienClicBlanc == 7 && ligneAncienClicBlanc == 7) petitRoqueBlancInterdit = true;
										if(colonneAncienClicBlanc == 0 && ligneAncienClicBlanc == 0) grandRoqueNoirInterdit = true;
										if(colonneAncienClicBlanc == 7 && ligneAncienClicBlanc == 0) petitRoqueNoirInterdit = true;
									}
									
									// Au tour de l'autre joueur de prendre le controle. 
									couleurControle = couleurAdverse;
									if(couleurControle.equals("blanc")) {
										couleurTourBlanc.setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("rondVert.png")));
										couleurTourNoir.setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("rondGris.png")));
									}
									else {
										couleurTourBlanc.setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("rondGris.png")));
										couleurTourNoir.setIcon(new ImageIcon(Fenetre.getRessourceAsBytes("rondVert.png")));
									}
								}
								aEuEchec = false;
								
								// Remise � z�ro de la pi�ce tampon, de son icone et de sa position. 
								// Ainsi que la potentielle pi�ce captur�e et de son icone. 
								laPiece = null;
								laPieceCapturee = null;
								lIcone = null;
								lIconeCapturee = null;
								laPosition = null;
							}
							// La case n'est pas atteignable par la pi�ce. 
							else {
								laPiece = null;
								lIcone = null;
								laPosition = null;
								setCursor(Cursor.getDefaultCursor());
								
								tabLabels[colonneClic][ligneClic].setBorder(new TitledBorder(BorderFactory.createLineBorder(Color.red, 5),
										"Impossible", TitledBorder.LEADING, TitledBorder.TOP, null, Color.red));
								tabLabels[colonneAncienClicBlanc][ligneAncienClicBlanc].setBorder(null);
								colonneAncienClicRouge = colonneClic;
								ligneAncienClicRouge = ligneClic;
							}
						}
					}
				}
			} else if(event.getSource() == reinePanel) {
				reinePanel.setBackground(Color.green);
				tourPanel.setBackground(colorBackground);
				fouPanel.setBackground(colorBackground);
				cavalierPanel.setBackground(colorBackground);
				
				aChoisiReine = true;
				aChoisiTour = false;
				aChoisiFou = false;
				aChoisiCavalier = false;
			} else if(event.getSource() == tourPanel) {
				reinePanel.setBackground(colorBackground);
				tourPanel.setBackground(Color.green);
				fouPanel.setBackground(colorBackground);
				cavalierPanel.setBackground(colorBackground);
				
				aChoisiReine = false;
				aChoisiTour = true;
				aChoisiFou = false;
				aChoisiCavalier = false;
			} else if(event.getSource() == fouPanel) {
				reinePanel.setBackground(colorBackground);
				tourPanel.setBackground(colorBackground);
				fouPanel.setBackground(Color.green);
				cavalierPanel.setBackground(colorBackground);
				
				aChoisiReine = false;
				aChoisiTour = false;
				aChoisiFou = true;
				aChoisiCavalier = false;
			} else if(event.getSource() == cavalierPanel) {
				reinePanel.setBackground(colorBackground);
				tourPanel.setBackground(colorBackground);
				fouPanel.setBackground(colorBackground);
				cavalierPanel.setBackground(Color.green);
				
				aChoisiReine = false;
				aChoisiTour = false;
				aChoisiFou = false;
				aChoisiCavalier = true;
			}
		}
		
		// Entr�e de la souris. 
		public void mouseEntered(MouseEvent event) {
			if(event.getSource() instanceof JLabel) {
				
			} else if(event.getSource() == boutonValiderNomJoueurs) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				surLeBouton = true;
				System.out.println(surLeBouton);
				int a=260, b=320, c=180, d=60;
				for(int i = 1; i < 6; i++) {
					a=a-1; b=b-1; c=c+2; d=d+2;
					boutonValiderNomJoueurs.setBounds(a, b, c, d);
				}
			} else if(event.getSource() == boutonLancer) {
				if(lancerEnable) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					surLeBouton = true;
					int a=66, b=7, c=50, d=50;
					for(int i = 1; i < 4; i++) {
						a=a-1; b=b-1; c=c+2; d=d+2;
						boutonLancer.setBounds(a, b, c, d);
					}
				} else {
					setCursor(Cursor.getDefaultCursor());
				}
			} else if(event.getSource() == boutonPause) {
				if(pauseEnable) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					surLeBouton = true;
					int a=166, b=7, c=50, d=50;
					for(int i = 1; i < 4; i++) {
						a=a-1; b=b-1; c=c+2; d=d+2;
						boutonPause.setBounds(a, b, c, d);
					}
				} else {
					setCursor(Cursor.getDefaultCursor());
				}
			} else if(event.getSource() == boutonRestart) {
				if(restartEnable) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					surLeBouton = true;
					int a=266, b=7, c=50, d=50;
					for(int i = 1; i < 4; i++) {
						a=a-1; b=b-1; c=c+2; d=d+2;
						boutonRestart.setBounds(a, b, c, d);
					}
				}
			} else if(event.getSource() == boutonAbandonner) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				surLeBouton = true;
				int a=1210, b=7, c=50, d=50;
				for(int i = 1; i < 4; i++) {
					a=a-1; b=b-1; c=c+2; d=d+2;
					boutonAbandonner.setBounds(a, b, c, d);
				}
			} else if(event.getSource() == boutonChangerNom) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				surLeBouton = true;
				//sonBoutonsControles.jouer();
				int a=1310, b=7, c=50, d=50;
				for(int i = 1; i < 4; i++) {
					a=a-1; b=b-1; c=c+2; d=d+2;
					boutonChangerNom.setBounds(a, b, c, d);
				}
			} else if(event.getSource() == boutonOptions) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				surLeBouton = true;
				//sonBoutonsControles.jouer();
				int a=1410, b=7, c=50, d=50;
				for(int i = 1; i < 4; i++) {
					a=a-1; b=b-1; c=c+2; d=d+2;
					boutonOptions.setBounds(a, b, c, d);
				}
			} else if(event.getSource() == boutonValider) {
				Fenetre.surLeBouton = true;
				int a=215, b=230, c=180, d=60;
				for(int i = 1; i < 6; i++) {
					a=a-1; b=b-1; c=c+2; d=d+2;
					boutonValider.setBounds(a, b, c, d);
				}
			}
		}
		
		// Sortie de la souris. 
		public void mouseExited(MouseEvent event) {
			if(event.getSource() instanceof JLabel) {
				
			} else if(event.getSource() == boutonValiderNomJoueurs) {
				setCursor(Cursor.getDefaultCursor());
				surLeBouton = false;
				int a=255, b=315, c=190, d=70;
				for(int i = 1; i < 6; i++) {
					a=a+1; b=b+1; c=c-2; d=d-2;
					boutonValiderNomJoueurs.setBounds(a, b, c, d);
				}
			} else if(event.getSource() == boutonLancer) {
				if(lancerEnable) {
					setCursor(Cursor.getDefaultCursor());
					surLeBouton = false;
					int a=63, b=4, c=56, d=56;
					for(int i = 1; i < 4; i++) {
						a=a+1; b=b+1; c=c-2; d=d-2;
						boutonLancer.setBounds(a, b, c, d);
					}
				} else {
					setCursor(Cursor.getDefaultCursor());
				}
			} else if(event.getSource() == boutonPause) {
				if(pauseEnable) {
					setCursor(Cursor.getDefaultCursor());
					surLeBouton = false;
					int a=163, b=4, c=56, d=56;
					for(int i = 1; i < 4; i++) {
						a=a+1; b=b+1; c=c-2; d=d-2;
						boutonPause.setBounds(a, b, c, d);
					}
				} else {
					setCursor(Cursor.getDefaultCursor());
				}
			} else if(event.getSource() == boutonRestart) {
				if(restartEnable) {
					setCursor(Cursor.getDefaultCursor());
					surLeBouton = false;
					int a=263, b=4, c=56, d=56;
					for(int i = 1; i < 4; i++) {
						a=a+1; b=b+1; c=c-2; d=d-2;
						boutonRestart.setBounds(a, b, c, d);
					}
				}
			} else if(event.getSource() == boutonAbandonner) {
				setCursor(Cursor.getDefaultCursor());
				surLeBouton = false;
				int a=1207, b=4, c=56, d=56;
				for(int i = 1; i < 4; i++) {
					a=a+1; b=b+1; c=c-2; d=d-2;
					boutonAbandonner.setBounds(a, b, c, d);
				}
			} else if(event.getSource() == boutonChangerNom) {
				setCursor(Cursor.getDefaultCursor());
				surLeBouton = false;
				int a=1307, b=4, c=56, d=56;
				for(int i = 1; i < 4; i++) {
					a=a+1; b=b+1; c=c-2; d=d-2;
					boutonChangerNom.setBounds(a, b, c, d);
				}
			} else if(event.getSource() == boutonOptions) {
				setCursor(Cursor.getDefaultCursor());
				surLeBouton = false;
				int a=1407, b=4, c=56, d=56;
				for(int i = 1; i < 4; i++) {
					a=a+1; b=b+1; c=c-2; d=d-2;
					boutonOptions.setBounds(a, b, c, d);
				}
			} else if(event.getSource() == boutonValider) {
				Fenetre.surLeBouton = false;
				int a=210, b=225, c=190, d=70;
				for(int i = 1; i < 6; i++) {
					a=a+1; b=b+1; c=c-2; d=d-2;
					boutonValider.setBounds(a, b, c, d);
				}
			}
		}
		
		// En train de cliquer. 
		public void mousePressed(MouseEvent event) {
			if(event.getSource() instanceof JLabel) {
				
			} else if(event.getSource() == boutonValiderNomJoueurs) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				surLeBouton = false;
				int a=255, b=315, c=190, d=70;
				for(int i = 1; i < 6; i++) {
					a=a+1; b=b+1; c=c-2; d=d-2;
					boutonValiderNomJoueurs.setBounds(a, b, c, d);
				}
			} else if(event.getSource() == boutonLancer) {
				if(lancerEnable) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					surLeBouton = false;
					int a=63, b=4, c=56, d=56;
					for(int i = 1; i < 4; i++) {
						a=a+1; b=b+1; c=c-2; d=d-2;
						boutonLancer.setBounds(a, b, c, d);
					}
				} else {
					setCursor(Cursor.getDefaultCursor());
				}
			} else if(event.getSource() == boutonPause) {
				if(pauseEnable) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					surLeBouton = false;
					int a=163, b=4, c=56, d=56;
					for(int i = 1; i < 4; i++) {
						a=a+1; b=b+1; c=c-2; d=d-2;
						boutonPause.setBounds(a, b, c, d);
					}
				} else {
					setCursor(Cursor.getDefaultCursor());
				}
			} else if(event.getSource() == boutonRestart) {
				if(restartEnable) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					surLeBouton = false;
					int a=263, b=4, c=56, d=56;
					for(int i = 1; i < 4; i++) {
						a=a+1; b=b+1; c=c-2; d=d-2;
						boutonRestart.setBounds(a, b, c, d);
					}
				}
			} else if(event.getSource() == boutonAbandonner) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				surLeBouton = false;
				int a=1207, b=4, c=56, d=56;
				for(int i = 1; i < 4; i++) {
					a=a+1; b=b+1; c=c-2; d=d-2;
					boutonAbandonner.setBounds(a, b, c, d);
				}
			} else if(event.getSource() == boutonChangerNom) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				surLeBouton = false;
				int a=1307, b=4, c=56, d=56;
				for(int i = 1; i < 4; i++) {
					a=a+1; b=b+1; c=c-2; d=d-2;
					boutonChangerNom.setBounds(a, b, c, d);
				}
			} else if(event.getSource() == boutonOptions) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				surLeBouton = false;
				int a=1407, b=4, c=56, d=56;
				for(int i = 1; i < 4; i++) {
					a=a+1; b=b+1; c=c-2; d=d-2;
					boutonOptions.setBounds(a, b, c, d);
				}
			} else if(event.getSource() == boutonValider) {
				Fenetre.surLeBouton = false;
				int a=210, b=225, c=190, d=70;
				for(int i = 1; i < 6; i++) {
					a=a+1; b=b+1; c=c-2; d=d-2;
					boutonValider.setBounds(a, b, c, d);
				}
			}
		}
		
		// Relachement du clic. 
		public void mouseReleased(MouseEvent event) {
			if(event.getSource() instanceof JLabel) {
				
			} else if(event.getSource() == boutonValiderNomJoueurs) {
				if(event.getX() > 0 && event.getX() < boutonValiderNomJoueurs.getWidth() && event.getY() > 0 && 
						event.getY() < boutonValiderNomJoueurs.getHeight()) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					surLeBouton = true;
					//sonBoutonsControles.jouer();
					int a=260, b=320, c=180, d=60;
					for(int i = 1; i < 6; i++) {
						a=a-1; b=b-1; c=c+2; d=d+2;
						boutonValiderNomJoueurs.setBounds(a, b, c, d);
					}
				}
			} else if(event.getSource() == boutonLancer) {
				if(lancerEnable) {
					if(event.getX() > 0 && event.getX() < boutonLancer.getWidth() && event.getY() > 0 && event.getY() < boutonLancer.getHeight()) {
						setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
						surLeBouton = true;
						//sonBoutonsControles.jouer();
						int a=66, b=7, c=50, d=50;
						for(int i = 1; i < 4; i++) {
							a=a-1; b=b-1; c=c+2; d=d+2;
							boutonLancer.setBounds(a, b, c, d);
						}
					}
				} else {
					setCursor(Cursor.getDefaultCursor());
				}
			} else if(event.getSource() == boutonPause) {
				if(pauseEnable) {
					if(event.getX() > 0 && event.getX() < boutonPause.getWidth() && event.getY() > 0 && event.getY() < boutonPause.getHeight()) { 
						setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
						surLeBouton = true;
						//sonBoutonsControles.jouer();
						int a=166, b=7, c=50, d=50;
						for(int i = 1; i < 4; i++) {
							a=a-1; b=b-1; c=c+2; d=d+2;
							boutonPause.setBounds(a, b, c, d);
						}
					}
				} else {
					setCursor(Cursor.getDefaultCursor());
				}
			} else if(event.getSource() == boutonRestart) {
				if(restartEnable) {
					if(event.getX() > 0 && event.getX() < boutonRestart.getWidth() && event.getY() > 0 && event.getY() < boutonRestart.getHeight()) {
						setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
						surLeBouton = true;
						//sonBoutonsControles.jouer();
						int a=266, b=7, c=50, d=50;
						for(int i = 1; i < 4; i++) {
							a=a-1; b=b-1; c=c+2; d=d+2;
							boutonRestart.setBounds(a, b, c, d);
						}
					}
				}
			} else if(event.getSource() == boutonAbandonner) {
				if(event.getX() > 0 && event.getX() < boutonAbandonner.getWidth() && event.getY() > 0 && event.getY() < boutonAbandonner.getHeight()) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					surLeBouton = true;
					//sonBoutonsControles.jouer();
					int a=1210, b=7, c=50, d=50;
					for(int i = 1; i < 4; i++) {
						a=a-1; b=b-1; c=c+2; d=d+2;
						boutonAbandonner.setBounds(a, b, c, d);
					}
				}
			} else if(event.getSource() == boutonChangerNom) {
				if(event.getX() > 0 && event.getX() < boutonChangerNom.getWidth() && event.getY() > 0 && event.getY() < boutonChangerNom.getHeight()) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					surLeBouton = true;
					//sonBoutonsControles.jouer();
					int a=1310, b=7, c=50, d=50;
					for(int i = 1; i < 4; i++) {
						a=a-1; b=b-1; c=c+2; d=d+2;
						boutonChangerNom.setBounds(a, b, c, d);
					}
				}
			} else if(event.getSource() == boutonOptions) {
				if(event.getX() > 0 && event.getX() < boutonOptions.getWidth() && event.getY() > 0 && event.getY() < boutonOptions.getHeight()) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					surLeBouton = true;
					//sonBoutonsControles.jouer();
					int a=1410, b=7, c=50, d=50;
					for(int i = 1; i < 4; i++) {
						a=a-1; b=b-1; c=c+2; d=d+2;
						boutonOptions.setBounds(a, b, c, d);
					}
				}
			} else if(event.getSource() == boutonValider) {
				if(event.getX() > 0 && event.getX() < boutonValider.getWidth() && event.getY() > 0 && event.getY() < boutonValider.getHeight()) {
					Fenetre.surLeBouton = true;
					int a=215, b=230, c=180, d=60;
					for(int i = 1; i < 6; i++) {
						a=a-1; b=b-1; c=c+2; d=d+2;
						boutonValider.setBounds(a, b, c, d);
					}
				}
			}
		}
	}
}




