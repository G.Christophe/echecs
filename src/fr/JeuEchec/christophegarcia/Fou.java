package fr.JeuEchec.christophegarcia;

public class Fou extends Piece {
	
	public Fou(String couleur) {
		super("fou", couleur);
	}
	
	public boolean estValide(Deplacement deplacement) {
		if(Math.abs(deplacement.getDeplacementX()) - Math.abs(deplacement.getDeplacementY()) == 0) {	// Déplacament diagonal seulement 														  // Pas de déplacement nul 
			return true;
		}
		return false;
	}
}
