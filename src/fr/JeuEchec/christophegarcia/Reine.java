package fr.JeuEchec.christophegarcia;

public class Reine extends Piece {
	
	public Reine(String couleur) {
		super("reine", couleur);
	}
	
	public boolean estValide(Deplacement deplacement) {
		if(Math.abs(deplacement.getDeplacementX()) - Math.abs(deplacement.getDeplacementY()) == 0 	// Déplacement diagonal 
			|| (deplacement.getDeplacementX() == 0 || deplacement.getDeplacementY() == 0)) {  		// OU déplacement rectiligne 
			return true;
		}
		return false;
	}
}
