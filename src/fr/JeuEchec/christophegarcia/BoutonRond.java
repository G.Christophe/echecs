package fr.JeuEchec.christophegarcia;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.RoundRectangle2D;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;

class BoutonRond extends JButton {
	private Image img;
	private String aNameString;

	public BoutonRond(String aNameString) {
		this.aNameString = aNameString;
		setContentAreaFilled(false);
		try {
			if (aNameString.equals("Fenetre.getRessourceAsBytes(\"valider.png\")"))
				img = ImageIO.read(Fenetre.class.getResourceAsStream("/valider.png"));
			else if (aNameString.equals("Fenetre.getRessourceAsBytes(\"play.png\")"))
				img = ImageIO.read(Fenetre.class.getResourceAsStream("/play.png"));
			else if (aNameString.equals("Fenetre.getRessourceAsBytes(\"pause.png\")"))
				img = ImageIO.read(Fenetre.class.getResourceAsStream("/pause.png"));
			else if (aNameString.equals("Fenetre.getRessourceAsBytes(\"restart.png\")"))
				img = ImageIO.read(Fenetre.class.getResourceAsStream("/restart.png"));
			else if (aNameString.equals("Fenetre.getRessourceAsBytes(\"abandonner.png\")"))
				img = ImageIO.read(Fenetre.class.getResourceAsStream("/abandonner.png"));
			else if (aNameString.equals("Fenetre.getRessourceAsBytes(\"editNom.png\")"))
				img = ImageIO.read(Fenetre.class.getResourceAsStream("/editNom.png"));
			else if (aNameString.equals("Fenetre.getRessourceAsBytes(\"parametresO.png\")"))
				img = ImageIO.read(Fenetre.class.getResourceAsStream("/parametresO.png"));
			else if (aNameString.equals("Fenetre.getRessourceAsBytes(\"annuler.png\")"))
				img = ImageIO.read(Fenetre.class.getResourceAsStream("/annuler.png"));
			else if (aNameString.equals("Fenetre.getRessourceAsBytes(\"abandonnerBouton.png\")"))
				img = ImageIO.read(Fenetre.class.getResourceAsStream("/abandonnerBouton.png"));
			else if (aNameString.equals("Fenetre.getRessourceAsBytes(\"pat.png\")"))
				img = ImageIO.read(Fenetre.class.getResourceAsStream("/pat.png"));
			else if (aNameString.equals("Fenetre.getRessourceAsBytes(\"echecEtMat.png\")"))
				img = ImageIO.read(Fenetre.class.getResourceAsStream("/echecEtMat.png"));
			else if (aNameString.equals("Fenetre.getRessourceAsBytes(\"boutonBlanc.png\")"))
				img = ImageIO.read(Fenetre.class.getResourceAsStream("/boutonBlanc.png"));
			else if (aNameString.equals("Fenetre.getRessourceAsBytes(\"boutonNoir.png\")"))
				img = ImageIO.read(Fenetre.class.getResourceAsStream("/boutonNoir.png"));
			else if (aNameString.equals("Fenetre.getRessourceAsBytes(\"orangeBouton.png\")"))
				img = ImageIO.read(Fenetre.class.getResourceAsStream("/orangeBouton.png"));
			else if (aNameString.equals("Fenetre.getRessourceAsBytes(\"vertBouton.png\")"))
				img = ImageIO.read(Fenetre.class.getResourceAsStream("/vertBouton.png"));
			else if (aNameString.equals("Fenetre.getRessourceAsBytes(\"marronBouton.png\")"))
				img = ImageIO.read(Fenetre.class.getResourceAsStream("/marronBouton.png"));
			else if (aNameString.equals("Fenetre.getRessourceAsBytes(\"bleuBouton.png\")"))
				img = ImageIO.read(Fenetre.class.getResourceAsStream("/bleuBouton.png"));
			else if (aNameString.equals("Fenetre.getRessourceAsBytes(\"violetBouton.png\")"))
				img = ImageIO.read(Fenetre.class.getResourceAsStream("/violetBouton.png"));
			else if (aNameString.equals("Fenetre.getRessourceAsBytes(\"grisBouton.png\")"))
				img = ImageIO.read(Fenetre.class.getResourceAsStream("/grisBouton.png"));
			else if (aNameString.equals("Fenetre.getRessourceAsBytes(\"turquoiseBouton.png\")"))
				img = ImageIO.read(Fenetre.class.getResourceAsStream("/turquoiseBouton.png"));
			else if (aNameString.equals("Fenetre.getRessourceAsBytes(\"rougeBouton.png\")"))
				img = ImageIO.read(Fenetre.class.getResourceAsStream("/rougeBouton.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		if (this.aNameString == "Fenetre.getRessourceAsBytes(\"valider.png\")") {
			Graphics2D g2d = (Graphics2D) g;
			RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 25, 25);
			g2d.clip(r2d);
			Color green = new Color(26, 225, 19);
			GradientPaint gradient = new GradientPaint(0, 0, green, 60, 60, green);
			g2d.setPaint(gradient);
			if (Fenetre.surLeBouton) {
				g2d.fillRect(0, 0, 220, 80);
			} else {
				g2d.fillRect(0, 0, 210, 70);
			}
			g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} else if (this.aNameString == "Fenetre.getRessourceAsBytes(\"play.png\")") {
			Graphics2D g2d = (Graphics2D) g;
			RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 25, 25);
			g2d.clip(r2d);
			Color green = new Color(26, 225, 19);
			GradientPaint gradient = new GradientPaint(0, 0, green, 50, 50, green);
			g2d.setPaint(gradient);
			if (Fenetre.surLeBouton) {
				g2d.fillRect(0, 0, 56, 56);
			} else {
				g2d.fillRect(0, 0, 50, 50);
			}
			g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} else if (this.aNameString == "Fenetre.getRessourceAsBytes(\"pause.png\")") {
			Graphics2D g2d = (Graphics2D) g;
			RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 25, 25);
			g2d.clip(r2d);
			Color green = new Color(225, 184, 19);
			GradientPaint gradient = new GradientPaint(0, 0, green, 50, 50, green);
			g2d.setPaint(gradient);
			if (Fenetre.surLeBouton) {
				g2d.fillRect(0, 0, 56, 56);
			} else {
				g2d.fillRect(0, 0, 50, 50);
			}
			g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} else if (this.aNameString == "Fenetre.getRessourceAsBytes(\"restart.png\")") {
			Graphics2D g2d = (Graphics2D) g;
			RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 25, 25);
			g2d.clip(r2d);
			Color red = new Color(225, 19, 19);
			GradientPaint gradient = new GradientPaint(0, 0, red, 50, 50, red);
			g2d.setPaint(gradient);
			if (Fenetre.surLeBouton) {
				g2d.fillRect(0, 0, 56, 56);
			} else {
				g2d.fillRect(0, 0, 50, 50);
			}
			g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} else if (this.aNameString == "Fenetre.getRessourceAsBytes(\"abandonner.png\")") {
			Graphics2D g2d = (Graphics2D) g;
			RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 25, 25);
			g2d.clip(r2d);
			GradientPaint gradient = new GradientPaint(0, 0, Color.darkGray, 50, 50, Color.darkGray);
			g2d.setPaint(gradient);
			if (Fenetre.surLeBouton) {
				g2d.fillRect(0, 0, 56, 56);
			} else {
				g2d.fillRect(0, 0, 50, 50);
			}
			g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} else if (this.aNameString == "Fenetre.getRessourceAsBytes(\"editNom.png\")") {
			Graphics2D g2d = (Graphics2D) g;
			RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 100, 100);
			g2d.clip(r2d);
			GradientPaint gradient = new GradientPaint(0, 0, Color.darkGray, 50, 50, Color.darkGray);
			g2d.setPaint(gradient);
			if (Fenetre.surLeBouton) {
				g2d.fillRect(0, 0, 56, 56);
			} else {
				g2d.fillRect(0, 0, 50, 50);
			}
			g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} else if (this.aNameString == "Fenetre.getRessourceAsBytes(\"parametresO.png\")") {
			Graphics2D g2d = (Graphics2D) g;
			RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 100, 100);
			g2d.clip(r2d);
			GradientPaint gradient = new GradientPaint(0, 0, Color.darkGray, 50, 50, Color.darkGray);
			g2d.setPaint(gradient);
			if (Fenetre.surLeBouton) {
				g2d.fillRect(0, 0, 56, 56);
			} else {
				g2d.fillRect(0, 0, 50, 50);
			}
			g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} else if (this.aNameString == "Fenetre.getRessourceAsBytes(\"annuler.png\")") {
			Graphics2D g2d = (Graphics2D) g;
			RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 25, 25);
			g2d.clip(r2d);
			Color red = new Color(225, 19, 19);
			GradientPaint gradient = new GradientPaint(0, 0, red, 60, 60, red);
			g2d.setPaint(gradient);
			if (Fenetre.surLeBouton) {
				g2d.fillRect(0, 0, 220, 80);
			} else {
				g2d.fillRect(0, 0, 210, 70);
			}
			g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} else if (this.aNameString == "Fenetre.getRessourceAsBytes(\"abandonnerBouton.png\")") {
			Graphics2D g2d = (Graphics2D) g;
			RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 25, 25);
			g2d.clip(r2d);
			GradientPaint gradient = new GradientPaint(0, 0, Color.white, 60, 60, Color.white);
			g2d.setPaint(gradient);
			if (Fenetre.surLeBouton) {
				g2d.fillRect(0, 0, 210, 110);
			} else {
				g2d.fillRect(0, 0, 200, 100);
			}
			g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} else if (this.aNameString == "Fenetre.getRessourceAsBytes(\"pat.png\")") {
			Graphics2D g2d = (Graphics2D) g;
			RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 25, 25);
			g2d.clip(r2d);
			GradientPaint gradient = new GradientPaint(0, 0, Color.white, 60, 60, Color.white);
			g2d.setPaint(gradient);
			if (Fenetre.surLeBouton) {
				g2d.fillRect(0, 0, 210, 110);
			} else {
				g2d.fillRect(0, 0, 200, 100);
			}
			g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} else if (this.aNameString == "Fenetre.getRessourceAsBytes(\"echecEtMat.png\")") {
			Graphics2D g2d = (Graphics2D) g;
			RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 25, 25);
			g2d.clip(r2d);
			GradientPaint gradient = new GradientPaint(0, 0, Color.white, 60, 60, Color.white);
			g2d.setPaint(gradient);
			if (Fenetre.surLeBouton) {
				g2d.fillRect(0, 0, 210, 110);
			} else {
				g2d.fillRect(0, 0, 200, 100);
			}
			g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} else if (this.aNameString == "Fenetre.getRessourceAsBytes(\"boutonBlanc.png\")") {
			Graphics2D g2d = (Graphics2D) g;
			RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 15, 15);
			g2d.clip(r2d);
			GradientPaint gradient = new GradientPaint(0, 0, Color.white, 60, 60, Color.white);
			g2d.setPaint(gradient);
			if (Fenetre.surLeBouton) {
				g2d.fillRect(0, 0, 220, 80);
			} else {
				g2d.fillRect(0, 0, 210, 70);
			}
			g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} else if (this.aNameString == "Fenetre.getRessourceAsBytes(\"boutonNoir.png\")") {
			Graphics2D g2d = (Graphics2D) g;
			RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 15, 15);
			g2d.clip(r2d);
			GradientPaint gradient = new GradientPaint(0, 0, Color.black, 60, 60, Color.black);
			g2d.setPaint(gradient);
			if (Fenetre.surLeBouton) {
				g2d.fillRect(0, 0, 220, 80);
			} else {
				g2d.fillRect(0, 0, 210, 70);
			}
			g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} else if (this.aNameString == "Fenetre.getRessourceAsBytes(\"orangeBouton.png\")") {
			Graphics2D g2d = (Graphics2D) g;
			RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 15, 15);
			g2d.clip(r2d);
			GradientPaint gradient = new GradientPaint(0, 0, Color.black, 60, 60, Color.black);
			g2d.setPaint(gradient);
			if (Fenetre.surLeBouton) {
				g2d.fillRect(0, 0, 220, 80);
			} else {
				g2d.fillRect(0, 0, 210, 70);
			}
			g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} else if (this.aNameString == "Fenetre.getRessourceAsBytes(\"vertBouton.png\")") {
			Graphics2D g2d = (Graphics2D) g;
			RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 15, 15);
			g2d.clip(r2d);
			GradientPaint gradient = new GradientPaint(0, 0, Color.black, 60, 60, Color.black);
			g2d.setPaint(gradient);
			if (Fenetre.surLeBouton) {
				g2d.fillRect(0, 0, 220, 80);
			} else {
				g2d.fillRect(0, 0, 210, 70);
			}
			g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} else if (this.aNameString == "Fenetre.getRessourceAsBytes(\"marronBouton.png\")") {
			Graphics2D g2d = (Graphics2D) g;
			RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 15, 15);
			g2d.clip(r2d);
			GradientPaint gradient = new GradientPaint(0, 0, Color.black, 60, 60, Color.black);
			g2d.setPaint(gradient);
			if (Fenetre.surLeBouton) {
				g2d.fillRect(0, 0, 220, 80);
			} else {
				g2d.fillRect(0, 0, 210, 70);
			}
			g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} else if (this.aNameString == "Fenetre.getRessourceAsBytes(\"bleuBouton.png\")") {
			Graphics2D g2d = (Graphics2D) g;
			RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 15, 15);
			g2d.clip(r2d);
			GradientPaint gradient = new GradientPaint(0, 0, Color.black, 60, 60, Color.black);
			g2d.setPaint(gradient);
			if (Fenetre.surLeBouton) {
				g2d.fillRect(0, 0, 220, 80);
			} else {
				g2d.fillRect(0, 0, 210, 70);
			}
			g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} else if (this.aNameString == "Fenetre.getRessourceAsBytes(\"violetBouton.png\")") {
			Graphics2D g2d = (Graphics2D) g;
			RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 15, 15);
			g2d.clip(r2d);
			GradientPaint gradient = new GradientPaint(0, 0, Color.black, 60, 60, Color.black);
			g2d.setPaint(gradient);
			if (Fenetre.surLeBouton) {
				g2d.fillRect(0, 0, 220, 80);
			} else {
				g2d.fillRect(0, 0, 210, 70);
			}
			g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} else if (this.aNameString == "Fenetre.getRessourceAsBytes(\"grisBouton.png\")") {
			Graphics2D g2d = (Graphics2D) g;
			RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 15, 15);
			g2d.clip(r2d);
			GradientPaint gradient = new GradientPaint(0, 0, Color.black, 60, 60, Color.black);
			g2d.setPaint(gradient);
			if (Fenetre.surLeBouton) {
				g2d.fillRect(0, 0, 220, 80);
			} else {
				g2d.fillRect(0, 0, 210, 70);
			}
			g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} else if (this.aNameString == "Fenetre.getRessourceAsBytes(\"turquoiseBouton.png\")") {
			Graphics2D g2d = (Graphics2D) g;
			RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 15, 15);
			g2d.clip(r2d);
			GradientPaint gradient = new GradientPaint(0, 0, Color.black, 60, 60, Color.black);
			g2d.setPaint(gradient);
			if (Fenetre.surLeBouton) {
				g2d.fillRect(0, 0, 220, 80);
			} else {
				g2d.fillRect(0, 0, 210, 70);
			}
			g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} else if (this.aNameString == "Fenetre.getRessourceAsBytes(\"rougeBouton.png\")") {
			Graphics2D g2d = (Graphics2D) g;
			RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 15, 15);
			g2d.clip(r2d);
			GradientPaint gradient = new GradientPaint(0, 0, Color.black, 60, 60, Color.black);
			g2d.setPaint(gradient);
			if (Fenetre.surLeBouton) {
				g2d.fillRect(0, 0, 220, 80);
			} else {
				g2d.fillRect(0, 0, 210, 70);
			}
			g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		}
	}
}
