package fr.JeuEchec.christophegarcia;

public class Tour extends Piece {
	
	public Tour(String couleur) {
		super("tour", couleur);
	}
	
	public boolean estValide(Deplacement deplacement) {
		if(deplacement.getDeplacementX() == 0 || deplacement.getDeplacementY() == 0) { 	 // Déplacement vertical ou horizontal seulement 
			return true;
		}
		return false;
	}
}
