package fr.JeuEchec.christophegarcia;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class FinDePartie extends JFrame {

	private JPanel panelPrincipal = new JPanel();
	private JPanel panelTitre = new JPanel();
	private JPanel panelAbandon = new JPanel();
	private JPanel panelMat = new JPanel();
	private JPanel panelEchecEtMat = new JPanel();
	private JPanel panelVNB = new JPanel();
	private JLabel labelTitre = new JLabel("Fin de la Partie ?");
	private JLabel labelAbandon = new JLabel("Abandon");
	private JLabel labelMat = new JLabel("Pat");
	private JLabel labelEchecEtMat = new JLabel("�chec & Mat");
	private JLabel labelVNB = new JLabel("Victoire du joueur");
	private Font policeTitre = new Font("Arial", Font.BOLD, 60); // police titre
	private Font policeMiniTitres = new Font("Arial", Font.BOLD, 40); // police des mini-titres
	private Font policeVictoireBN = new Font("Arial", Font.BOLD, 20); // police du texte "Victoire du joueur"

	static JButton boutonAnnuler = new BoutonRond("Fenetre.getRessourceAsBytes(\"annuler.png\")");
	static JButton boutonAbandon = new BoutonRond("Fenetre.getRessourceAsBytes(\"abandonnerBouton.png\")");
	static JButton boutonPat = new BoutonRond("Fenetre.getRessourceAsBytes(\"pat.png\")");
	static JButton boutonEchecEtMat = new BoutonRond("Fenetre.getRessourceAsBytes(\"echecEtMat.png\")");
	static JButton boutonBlanc = new BoutonRond("Fenetre.getRessourceAsBytes(\"boutonBlanc.png\")");
	static JButton boutonNoir = new BoutonRond("Fenetre.getRessourceAsBytes(\"boutonNoir.png\")");

	private Color couleurFond = new Color(100, 100, 100);

	/**
	 * Constructor
	 */
	public FinDePartie() {
		// Initialisation de la fenetre.
		try {
			jbInit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Initializes all
	 * 
	 * @throws Exception - exception
	 */
	private void jbInit() throws Exception {
		// icon parameters
		this.setIconImage(new ImageIcon(Fenetre.getRessourceAsBytes("icon.png")).getImage());

		// parametres fenetre
		this.setTitle("Fin de la Partie ?");
		this.setSize(900, 500);
		this.setUndecorated(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setContentPane(panelPrincipal);

		// Les �couteurs
		EvenementsSouris gestEvent = new EvenementsSouris();
		boutonAnnuler.addMouseListener(gestEvent);
		boutonAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Fenetre.boutonLancer.setEnabled(true);
				Fenetre.boutonPause.setEnabled(false);
				Fenetre.lancerEnable = true;
				Fenetre.pauseEnable = false;
				Fenetre.champTemps.setText("Jeu en Pause");

				dispose();
			}
		});
		boutonAbandon.addMouseListener(gestEvent);
		boutonAbandon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Fenetre.affichageCoupSpecial = true;
				Fenetre.titreAffichage = "Abandon";

				if (!Fenetre.preparationPiecesMortesFaite) {
					Fenetre.nomJoueurGauche.setForeground(Color.green);
					Fenetre.nomJoueurDroite.setForeground(Color.green);
				} else {
					if (Fenetre.couleurControle.equals("blanc")) {
						Fenetre.joueurGaucheAGagne = false;
						Fenetre.nomJoueurGauche.setForeground(Color.red);
						Fenetre.nomJoueurDroite.setForeground(Color.green);
					} else {
						Fenetre.joueurGaucheAGagne = true;
						Fenetre.nomJoueurGauche.setForeground(Color.green);
						Fenetre.nomJoueurDroite.setForeground(Color.red);
					}
				}

				Fenetre.boutonLancer.setEnabled(false);
				Fenetre.boutonPause.setEnabled(false);
				Fenetre.lancerEnable = false;
				Fenetre.pauseEnable = true;

				Fenetre.champTemps.setForeground(Color.white);
				Font fontAffichageTemps = new Font("Arial", Font.ITALIC, 20);
				String temps = ("(Temps : " + (Fenetre.heures < 10 ? "" : Fenetre.heures + "h ")
						+ (Fenetre.minutes < 10 ? "0" + Fenetre.minutes + "m " : Fenetre.minutes + "m ")
						+ (Fenetre.secondes < 10 ? "0" + Fenetre.secondes + "s " : Fenetre.secondes + "s") + ")");
				JLabel affichageTemps = new JLabel(temps);
				Fenetre.champTemps.setBounds(471, 0, 585, 40);
				affichageTemps.setBounds(471, 40, 585, 20);
				affichageTemps.setFont(fontAffichageTemps);
				affichageTemps.setForeground(new Color(255, 255, 255));
				affichageTemps.setHorizontalAlignment(SwingConstants.CENTER);
				Fenetre.panelControles.add(affichageTemps);
				Fenetre.champTemps.setText("F�licitations � vous 2 !");

				dispose();
			}
		});
		boutonPat.addMouseListener(gestEvent);
		boutonPat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Fenetre.affichageCoupSpecial = true;
				Fenetre.titreAffichage = "PAT";

				Fenetre.nomJoueurGauche.setForeground(Color.green);
				Fenetre.nomJoueurDroite.setForeground(Color.green);

				Fenetre.boutonLancer.setEnabled(false);
				Fenetre.boutonPause.setEnabled(false);
				Fenetre.lancerEnable = false;
				Fenetre.pauseEnable = true;

				Fenetre.champTemps.setForeground(Color.white);
				Font fontAffichageTemps = new Font("Arial", Font.ITALIC, 20);
				String temps = ("(Temps : " + (Fenetre.heures < 10 ? "" : Fenetre.heures + "h ")
						+ (Fenetre.minutes < 10 ? "0" + Fenetre.minutes + "m " : Fenetre.minutes + "m ")
						+ (Fenetre.secondes < 10 ? "0" + Fenetre.secondes + "s " : Fenetre.secondes + "s") + ")");
				JLabel affichageTemps = new JLabel(temps);
				Fenetre.champTemps.setBounds(471, 0, 585, 40);
				affichageTemps.setBounds(471, 40, 585, 20);
				affichageTemps.setFont(fontAffichageTemps);
				affichageTemps.setForeground(new Color(255, 255, 255));
				affichageTemps.setHorizontalAlignment(SwingConstants.CENTER);
				Fenetre.panelControles.add(affichageTemps);
				Fenetre.champTemps.setText("F�licitations � vous 2 !");

				dispose();
			}
		});
		boutonEchecEtMat.addMouseListener(gestEvent);
		boutonEchecEtMat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				victoireVisible();
			}
		});
		boutonBlanc.addMouseListener(gestEvent);
		boutonBlanc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Fenetre.affichageCoupSpecial = true;
				Fenetre.titreAffichage = "ECHEC & MAT";
				Fenetre.joueurGaucheAGagne = true;

				Fenetre.nomJoueurGauche.setForeground(Color.green);
				Fenetre.nomJoueurDroite.setForeground(Color.red);

				Fenetre.boutonLancer.setEnabled(false);
				Fenetre.boutonPause.setEnabled(false);
				Fenetre.lancerEnable = false;
				Fenetre.pauseEnable = true;

				Fenetre.champTemps.setForeground(Color.white);
				Font fontAffichageTemps = new Font("Arial", Font.ITALIC, 20);
				String temps = ("(Temps : " + (Fenetre.heures < 10 ? "" : Fenetre.heures + "h ")
						+ (Fenetre.minutes < 10 ? "0" + Fenetre.minutes + "m " : Fenetre.minutes + "m ")
						+ (Fenetre.secondes < 10 ? "0" + Fenetre.secondes + "s " : Fenetre.secondes + "s") + ")");
				JLabel affichageTemps = new JLabel(temps);
				Fenetre.champTemps.setBounds(471, 0, 585, 40);
				affichageTemps.setBounds(471, 40, 585, 20);
				affichageTemps.setFont(fontAffichageTemps);
				affichageTemps.setForeground(new Color(255, 255, 255));
				affichageTemps.setHorizontalAlignment(SwingConstants.CENTER);
				Fenetre.panelControles.add(affichageTemps);
				Fenetre.champTemps.setText("F�licitations � vous 2 !");

				dispose();
			}
		});
		boutonNoir.addMouseListener(gestEvent);
		boutonNoir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Fenetre.affichageCoupSpecial = true;
				Fenetre.titreAffichage = "ECHEC & MAT";
				Fenetre.joueurGaucheAGagne = false;

				Fenetre.nomJoueurGauche.setForeground(Color.red);
				Fenetre.nomJoueurDroite.setForeground(Color.green);

				Fenetre.boutonLancer.setEnabled(false);
				Fenetre.boutonPause.setEnabled(false);
				Fenetre.lancerEnable = false;
				Fenetre.pauseEnable = true;

				Fenetre.champTemps.setForeground(Color.white);
				Font fontAffichageTemps = new Font("Arial", Font.ITALIC, 20);
				String temps = ("(Temps : " + (Fenetre.heures < 10 ? "" : Fenetre.heures + "h ")
						+ (Fenetre.minutes < 10 ? "0" + Fenetre.minutes + "m " : Fenetre.minutes + "m ")
						+ (Fenetre.secondes < 10 ? "0" + Fenetre.secondes + "s " : Fenetre.secondes + "s") + ")");
				JLabel affichageTemps = new JLabel(temps);
				Fenetre.champTemps.setBounds(471, 0, 585, 40);
				affichageTemps.setBounds(471, 40, 585, 20);
				affichageTemps.setFont(fontAffichageTemps);
				affichageTemps.setForeground(new Color(255, 255, 255));
				affichageTemps.setHorizontalAlignment(SwingConstants.CENTER);
				Fenetre.panelControles.add(affichageTemps);
				Fenetre.champTemps.setText("F�licitations � vous 2 !");

				dispose();
			}
		});

		// Panel Principal.
		panelPrincipal.setLayout(null);
		panelPrincipal.setBackground(couleurFond);
		panelPrincipal.add(panelTitre, null);
		panelPrincipal.add(panelAbandon, null);
		panelPrincipal.add(panelMat, null);
		panelPrincipal.add(panelEchecEtMat, null);
		panelPrincipal.add(panelVNB, null);
		panelPrincipal.add(boutonAbandon, null);
		panelPrincipal.add(boutonPat, null);
		panelPrincipal.add(boutonEchecEtMat, null);
		panelPrincipal.add(boutonBlanc, null);
		panelPrincipal.add(boutonNoir, null);
		panelPrincipal.add(boutonAnnuler, null);

		// Panel Titre "Fin de la Partie ?".
		panelTitre.setBackground(couleurFond);
		panelTitre.setBounds(0, 20, 900, 130);
		panelTitre.add(labelTitre);
		labelTitre.setForeground(Color.white);
		labelTitre.setFont(policeTitre);

		// Panel Texte "Abandon".
		panelAbandon.setBackground(couleurFond);
		panelAbandon.setBounds(0, 150, 300, 80);
		panelAbandon.add(labelAbandon);
		labelAbandon.setForeground(Color.white);
		labelAbandon.setFont(policeMiniTitres);

		// Panel Texte "Mat".
		panelMat.setBackground(couleurFond);
		panelMat.setBounds(300, 150, 300, 80);
		panelMat.add(labelMat);
		labelMat.setForeground(Color.white);
		labelMat.setFont(policeMiniTitres);

		// Panel Texte "�chec & Mat".
		panelEchecEtMat.setBackground(couleurFond);
		panelEchecEtMat.setBounds(600, 150, 300, 80);
		panelEchecEtMat.add(labelEchecEtMat);
		labelEchecEtMat.setForeground(Color.white);
		labelEchecEtMat.setFont(policeMiniTitres);

		// Panel Texte "Victoire du Joueur".
		panelVNB.setBackground(couleurFond);
		panelVNB.setBounds(600, 345, 300, 30);
		panelVNB.add(labelVNB);
		labelVNB.setForeground(Color.white);
		labelVNB.setFont(policeVictoireBN);

		// Bouton Annuler.
		boutonAnnuler.setBounds(360, 395, 180, 60);
		boutonAnnuler.setBorderPainted(false);
		boutonAnnuler.setFocusPainted(false);

		// Bouton Abondon.
		boutonAbandon.setBounds(50, 240, 200, 100);
		boutonAbandon.setBorderPainted(false);
		boutonAbandon.setFocusPainted(false);

		// Bouton Mat.
		boutonPat.setBounds(350, 240, 200, 100);
		boutonPat.setBorderPainted(false);
		boutonPat.setFocusPainted(false);

		// Bouton Echec Et Mat.
		boutonEchecEtMat.setBounds(650, 240, 200, 100);
		boutonEchecEtMat.setBorderPainted(false);
		boutonEchecEtMat.setFocusPainted(false);

		// Bouton Blanc (visible et cliquable si le joueur a cliqu� sur le bouton �chec
		// & Mat).
		boutonBlanc.setBounds(655, 380, 90, 40);
		boutonBlanc.setBorderPainted(false);
		boutonBlanc.setFocusPainted(false);

		// Bouton Noir (visible et cliquable si le joueur a cliqu� sur le bouton �chec &
		// Mat).
		boutonNoir.setBounds(755, 380, 90, 40);
		boutonNoir.setBorderPainted(false);
		boutonNoir.setFocusPainted(false);

		this.setVisible(true);
		panelVNB.setVisible(false);
		boutonBlanc.setVisible(false);
		boutonNoir.setVisible(false);
	}

	public void victoireVisible() {
		panelVNB.setVisible(true);
		boutonBlanc.setVisible(true);
		boutonNoir.setVisible(true);
	}

	private class EvenementsSouris extends MouseAdapter {

		// Entr�e de la souris.
		public void mouseEntered(MouseEvent event) {
			if (event.getSource() == boutonAbandon) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 50, b = 240, c = 200, d = 100;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					boutonAbandon.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == boutonPat) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 350, b = 240, c = 200, d = 100;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					boutonPat.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == boutonEchecEtMat) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 650, b = 240, c = 200, d = 100;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					boutonEchecEtMat.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == boutonBlanc) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 655, b = 380, c = 90, d = 40;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					boutonBlanc.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == boutonNoir) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 755, b = 380, c = 90, d = 40;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					boutonNoir.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == boutonAnnuler) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 360, b = 395, c = 180, d = 60;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					boutonAnnuler.setBounds(a, b, c, d);
				}
			}
		}

		// Sortie de la souris.
		public void mouseExited(MouseEvent event) {
			if (event.getSource() == boutonAbandon) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = false;
				int a = 45, b = 235, c = 210, d = 110;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					boutonAbandon.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == boutonPat) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = false;
				int a = 345, b = 235, c = 210, d = 110;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					boutonPat.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == boutonEchecEtMat) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = false;
				int a = 645, b = 235, c = 210, d = 110;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					boutonEchecEtMat.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == boutonBlanc) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = false;
				int a = 650, b = 375, c = 100, d = 50;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					boutonBlanc.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == boutonNoir) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = false;
				int a = 750, b = 375, c = 100, d = 50;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					boutonNoir.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == boutonAnnuler) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = false;
				int a = 355, b = 390, c = 190, d = 70;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					boutonAnnuler.setBounds(a, b, c, d);
				}
			}
		}

		// En train de cliquer sur le bouton.
		public void mousePressed(MouseEvent event) {
			if (event.getSource() == boutonAbandon) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = false;
				int a = 45, b = 235, c = 210, d = 110;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					boutonAbandon.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == boutonPat) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = false;
				int a = 345, b = 235, c = 210, d = 110;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					boutonPat.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == boutonEchecEtMat) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = false;
				int a = 645, b = 235, c = 210, d = 110;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					boutonEchecEtMat.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == boutonBlanc) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = false;
				int a = 650, b = 375, c = 100, d = 50;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					boutonBlanc.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == boutonNoir) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = false;
				int a = 750, b = 375, c = 100, d = 50;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					boutonNoir.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == boutonAnnuler) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = false;
				int a = 355, b = 390, c = 190, d = 70;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					boutonAnnuler.setBounds(a, b, c, d);
				}
			}
		}

		// Relacher le clic du bouton.
		public void mouseReleased(MouseEvent event) {
			if (event.getSource() == boutonAbandon) {
				if (event.getX() > 0 && event.getX() < boutonAbandon.getWidth() && event.getY() > 0
						&& event.getY() < boutonAbandon.getHeight()) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					Fenetre.surLeBouton = true;
					int a = 50, b = 240, c = 200, d = 100;
					for (int i = 1; i < 6; i++) {
						a = a - 1;
						b = b - 1;
						c = c + 2;
						d = d + 2;
						boutonAbandon.setBounds(a, b, c, d);
					}
				}
			} else if (event.getSource() == boutonPat) {
				if (event.getX() > 0 && event.getX() < boutonPat.getWidth() && event.getY() > 0
						&& event.getY() < boutonPat.getHeight()) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					Fenetre.surLeBouton = true;
					int a = 350, b = 240, c = 200, d = 100;
					for (int i = 1; i < 6; i++) {
						a = a - 1;
						b = b - 1;
						c = c + 2;
						d = d + 2;
						boutonPat.setBounds(a, b, c, d);
					}
				}
			} else if (event.getSource() == boutonEchecEtMat) {
				if (event.getX() > 0 && event.getX() < boutonEchecEtMat.getWidth() && event.getY() > 0
						&& event.getY() < boutonEchecEtMat.getHeight()) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					Fenetre.surLeBouton = true;
					int a = 650, b = 240, c = 200, d = 100;
					for (int i = 1; i < 6; i++) {
						a = a - 1;
						b = b - 1;
						c = c + 2;
						d = d + 2;
						boutonEchecEtMat.setBounds(a, b, c, d);
					}
				}
			} else if (event.getSource() == boutonBlanc) {
				if (event.getX() > 0 && event.getX() < boutonBlanc.getWidth() && event.getY() > 0
						&& event.getY() < boutonBlanc.getHeight()) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					Fenetre.surLeBouton = true;
					int a = 655, b = 380, c = 90, d = 40;
					for (int i = 1; i < 6; i++) {
						a = a - 1;
						b = b - 1;
						c = c + 2;
						d = d + 2;
						boutonBlanc.setBounds(a, b, c, d);
					}
				}
			} else if (event.getSource() == boutonNoir) {
				if (event.getX() > 0 && event.getX() < boutonNoir.getWidth() && event.getY() > 0
						&& event.getY() < boutonNoir.getHeight()) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					Fenetre.surLeBouton = true;
					int a = 755, b = 380, c = 90, d = 40;
					for (int i = 1; i < 6; i++) {
						a = a - 1;
						b = b - 1;
						c = c + 2;
						d = d + 2;
						boutonNoir.setBounds(a, b, c, d);
					}
				}
			} else if (event.getSource() == boutonAnnuler) {
				if (event.getX() > 0 && event.getX() < boutonAnnuler.getWidth() && event.getY() > 0
						&& event.getY() < boutonAnnuler.getHeight()) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					Fenetre.surLeBouton = true;
					int a = 360, b = 395, c = 180, d = 60;
					for (int i = 1; i < 6; i++) {
						a = a - 1;
						b = b - 1;
						c = c + 2;
						d = d + 2;
						boutonAnnuler.setBounds(a, b, c, d);
					}
				}
			}
		}
	}
}
