package fr.JeuEchec.christophegarcia;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class NomJoueurs extends JFrame {
	static Fenetre fenetreJeu = null;

	private JPanel panelPrincipalNomJoueurs = new JPanel();
	private JPanel panelTitre = new JPanel();
	private JPanel panelContrainte = new JPanel();
	private JPanel panelNomJoueur1 = new JPanel();
	private JPanel panelNomJoueur2 = new JPanel();
	private JTextField jtfNomJoueur1 = new JTextField(); // ou ici peut etre le UPPER(String) ...
	private JTextField jtfNomJoueur2 = new JTextField(); // ou ici peut etre le UPPER(String) ...
	private JLabel labelTitre = new JLabel("NOM DE NOS 2 CHAMPIONS");
	private JLabel labelContrainte = new JLabel("8 caract�res maximum");
	private JLabel labelNomJoueur1 = new JLabel("Pseudo du joueur 1 : ");
	private JLabel labelNomJoueur2 = new JLabel("Pseudo du joueur 2 : ");
	private Font policeJTF = new Font("Arial", Font.BOLD, 75);
	private Font policeTitre = new Font("Arial", Font.BOLD, 40); // police titre
	private Font policeLabels = new Font("Arial", Font.BOLD, 20); // police 2 autres labels

	static JButton boutonValiderNomJoueurs = new BoutonRond("Fenetre.getRessourceAsBytes(\"valider.png\")"); // Bouton
																												// pour
																												// valider
																												// certains
																												// param�tres

	private String nomJoueur1;
	private String nomJoueur2;

	/**
	 * Constructor
	 */
	public NomJoueurs() {
		// Initialisation de la fenetre.
		try {
			jbInit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Initializes all
	 * 
	 * @throws Exception - exception
	 */
	private void jbInit() throws Exception {
		// icon parameters
		this.setIconImage(new ImageIcon(Fenetre.getRessourceAsBytes("icon.png")).getImage());

		// Param�tres fen�tre
		this.setSize(700, 400);
		this.setTitle("Nom de nos Champions");
		this.setUndecorated(true);
		this.setDefaultCloseOperation(NomJoueurs.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setContentPane(panelPrincipalNomJoueurs);

		// les �couteurs
		EvenementsSouris gestEvent = new EvenementsSouris();
		boutonValiderNomJoueurs.addMouseListener(gestEvent);
		boutonValiderNomJoueurs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jtfNomJoueur1.getText().length() > 0 && jtfNomJoueur1.getText().length() <= 8
						&& jtfNomJoueur2.getText().length() > 0 && jtfNomJoueur2.getText().length() <= 8) {
					setNomJoueur1(jtfNomJoueur1.getText().toUpperCase());
					setNomJoueur2(jtfNomJoueur2.getText().toUpperCase());
					fenetreJeu = new Fenetre(nomJoueur1, nomJoueur2);
					dispose();
				}
			}
		});

		// Panel Principal
		panelPrincipalNomJoueurs.setBackground(new Color(100, 100, 100));
		panelPrincipalNomJoueurs.setLayout(null);
		panelPrincipalNomJoueurs.add(panelTitre, null);
		panelPrincipalNomJoueurs.add(panelContrainte, null);
		panelPrincipalNomJoueurs.add(panelNomJoueur1, null);
		panelPrincipalNomJoueurs.add(panelNomJoueur2, null);
		panelPrincipalNomJoueurs.add(boutonValiderNomJoueurs, null);

		// Panel Titre "NOM DE NOS 2 CHAMPIONS"
		panelTitre.setBackground(new Color(100, 100, 100));
		panelTitre.setBounds(0, 0, 700, 50);
		panelTitre.add(labelTitre);
		labelTitre.setForeground(Color.white);
		labelTitre.setFont(policeTitre);

		// Panel de la contrainte (8 caract�res)
		panelContrainte.setBackground(new Color(100, 100, 100));
		panelContrainte.setBounds(0, 50, 700, 50);
		panelContrainte.add(labelContrainte);
		labelContrainte.setForeground(Color.red);
		labelContrainte.setFont(policeLabels);

		// Panel Joueur 1
		panelNomJoueur1.setBackground(new Color(100, 100, 100));
		panelNomJoueur1.add(labelNomJoueur1, null);
		panelNomJoueur1.add(jtfNomJoueur1, null);
		panelNomJoueur1.setBounds(0, 90, 700, 100);
		jtfNomJoueur1.setText("Joueur 1");
		jtfNomJoueur1.setFont(policeJTF);
		jtfNomJoueur1.setPreferredSize(new Dimension(470, 70));
		jtfNomJoueur1.setForeground(new Color(253, 206, 158));
		jtfNomJoueur1.addMouseListener(gestEvent);
		labelNomJoueur1.setForeground(Color.white);
		labelNomJoueur1.setFont(policeLabels);

		// Panel Joueur 2
		panelNomJoueur2.setBackground(new Color(100, 100, 100));
		panelNomJoueur2.add(labelNomJoueur2, null);
		panelNomJoueur2.add(jtfNomJoueur2, null);
		panelNomJoueur2.setBounds(0, 190, 700, 100);
		jtfNomJoueur2.setText("Joueur 2");
		jtfNomJoueur2.setFont(policeJTF);
		jtfNomJoueur2.setPreferredSize(new Dimension(470, 70));
		jtfNomJoueur2.setForeground(new Color(253, 206, 158));
		jtfNomJoueur2.addMouseListener(gestEvent);
		labelNomJoueur2.setForeground(Color.white);
		labelNomJoueur2.setFont(policeLabels);

		// Bouton Valider
		boutonValiderNomJoueurs.setBounds(260, 320, 180, 60);
		boutonValiderNomJoueurs.setBorderPainted(false);
		boutonValiderNomJoueurs.setFocusPainted(false);

		this.setVisible(true);
	}

	public void setNomJoueur1(String nomJoueur1) {
		if (nomJoueur1 != null) {
			this.nomJoueur1 = nomJoueur1;
		}
	}

	public void setNomJoueur2(String nomJoueur2) {
		if (nomJoueur2 != null) {
			this.nomJoueur2 = nomJoueur2;
		}
	}

	public String getNomJoueur(String joueur) {
		if (joueur == "joueur1") {
			return nomJoueur1;
		} else if (joueur == "joueur2") {
			return nomJoueur2;
		} else {
			System.err.println("NomJoueurs : getNomJoueur : joueur != \"joueur1\" && joueur != \"joueur2\".");
			return "";
		}
	}

	private class EvenementsSouris extends MouseAdapter {

		// Entr�e de la souris.
		public void mouseEntered(MouseEvent event) {
			if (event.getSource() == boutonValiderNomJoueurs) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 260, b = 320, c = 180, d = 60;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					boutonValiderNomJoueurs.setBounds(a, b, c, d);
				}
			}
		}

		// Sortie de la souris.
		public void mouseExited(MouseEvent event) {
			if (event.getSource() == boutonValiderNomJoueurs) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = false;
				int a = 255, b = 315, c = 190, d = 70;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					boutonValiderNomJoueurs.setBounds(a, b, c, d);
				}
			}
		}

		// En train de cliquer sur le bouton.
		public void mousePressed(MouseEvent event) {
			if (event.getSource() == boutonValiderNomJoueurs) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = false;
				int a = 255, b = 315, c = 190, d = 70;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					boutonValiderNomJoueurs.setBounds(a, b, c, d);
				}
			}
		}

		// Relacher le clic du bouton.
		public void mouseReleased(MouseEvent event) {
			if (event.getSource() == boutonValiderNomJoueurs) {
				if (event.getX() > 0 && event.getX() < boutonValiderNomJoueurs.getWidth() && event.getY() > 0
						&& event.getY() < boutonValiderNomJoueurs.getHeight()) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					Fenetre.surLeBouton = true;
					int a = 260, b = 320, c = 180, d = 60;
					for (int i = 1; i < 6; i++) {
						a = a - 1;
						b = b - 1;
						c = c + 2;
						d = d + 2;
						boutonValiderNomJoueurs.setBounds(a, b, c, d);
					}
				}
			}
		}

		// Clic de la souris.
		public void mouseClicked(MouseEvent event) {
			if (event.getSource() == jtfNomJoueur1) {
				jtfNomJoueur1.setText("");
				jtfNomJoueur1.removeMouseListener(this);
			} else if (event.getSource() == jtfNomJoueur2) {
				jtfNomJoueur2.setText("");
				jtfNomJoueur2.removeMouseListener(this);
			}
		}
	}
}
