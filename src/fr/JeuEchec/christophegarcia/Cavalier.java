package fr.JeuEchec.christophegarcia;

public class Cavalier extends Piece {
	
	public Cavalier(String couleur) {
		super("cavalier", couleur);
	}
	
	public boolean estValide(Deplacement deplacement) {
		if((Math.abs(deplacement.getDeplacementX() / deplacement.getDeplacementY()) == 2 || 	// D�placement en L (droite ou gauche) 
		    Math.abs(deplacement.getDeplacementX() / deplacement.getDeplacementY()) == 0.5)  	// D�placement en L (en haut ou en bas) 
			&& Math.abs(deplacement.getDeplacementX()) <= 2 									// Pas de d�placement plus loin que pr�vu 
			&& Math.abs(deplacement.getDeplacementY()) <= 2) { 									// Pas de d�placement plus loin que pr�vu 																		// Pas de d�placement nul 
			return true;
		}
		return false;
	}
}
