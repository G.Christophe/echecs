package fr.JeuEchec.christophegarcia;

/* mettre :
	� couper son musique / r�activer son musique (quand couper : musique en pause, quand r�activer : musique reprend)
		---> (symobole volume coup� / symobole volume activ�) 
	
	� augmenter / baisser le volume de la musique 
		---> (symbole volume faible) -------------------0------- (symbole volume fort)
	
	� Proposer les 8 choix de couleurs de l'�chiquier avec bouton image :
		---> 
			________	________	________	________ 
			|	|	|	|	|	|	|	|	|	|	|	|
			|___|___|	|___|___|	|___|___|	|___|___|
			|	|	|	|	|	|	|	|	|	|	|	|
			|___|___|	|___|___|	|___|___|	|___|___|
			
			________	________	________	________ 
			|	|	|	|	|	|	|	|	|	|	|	|
			|___|___|	|___|___|	|___|___|	|___|___|
			|	|	|	|	|	|	|	|	|	|	|	|
			|___|___|	|___|___|	|___|___|	|___|___|
	
	
*/

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Parametres extends JFrame {
	private JPanel panelPrincipalParametres = new JPanel();
	private JPanel panelTitre = new JPanel();
	private JPanel panelSonTitre = new JPanel();
	private JPanel panelEchiquierTitre = new JPanel();
	private JLabel labelTitre = new JLabel("PARAM�TRES");
	private JLabel labelSon = new JLabel("Son : ");
	private JLabel labelEchiquier = new JLabel("�chiquier : ");
	private Font policeTitre = new Font("Arial", Font.BOLD, 60); // police titre
	private Font policeLabels = new Font("Arial", Font.BOLD, 30); // police 2 autres labels

	static JButton boutonValider = new BoutonRond("Fenetre.getRessourceAsBytes(\"valider.png\")"); // Bouton pour
																									// valider les
																									// param�tres
	static JButton orangeBouton = new BoutonRond("Fenetre.getRessourceAsBytes(\"orangeBouton.png\")");
	static JButton vertBouton = new BoutonRond("Fenetre.getRessourceAsBytes(\"vertBouton.png\")");
	static JButton marronBouton = new BoutonRond("Fenetre.getRessourceAsBytes(\"marronBouton.png\")");
	static JButton bleuBouton = new BoutonRond("Fenetre.getRessourceAsBytes(\"bleuBouton.png\")");
	static JButton violetBouton = new BoutonRond("Fenetre.getRessourceAsBytes(\"violetBouton.png\")");
	static JButton grisBouton = new BoutonRond("Fenetre.getRessourceAsBytes(\"grisBouton.png\")");
	static JButton turquoiseBouton = new BoutonRond("Fenetre.getRessourceAsBytes(\"turquoiseBouton.png\")");
	static JButton rougeBouton = new BoutonRond("Fenetre.getRessourceAsBytes(\"rougeBouton.png\")");

	/**
	 * Constructor
	 */
	public Parametres() {

		// Initialisation de la fen�tre.
		try {
			jbInit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Initializes all
	 * 
	 * @throws Exception - exception
	 */
	private void jbInit() throws Exception {
		// Param�tres Icon.
		this.setIconImage(new ImageIcon(Fenetre.getRessourceAsBytes("icon.png")).getImage());

		// Param�tres fen�tre.
		this.setSize(850, 700);
		this.setTitle("Param�tres");
		this.setUndecorated(true);
		this.setDefaultCloseOperation(NomJoueurs.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setContentPane(panelPrincipalParametres);

		// Les �couteurs.
		EvenementsSouris gestEvent = new EvenementsSouris();
		boutonValider.addMouseListener(gestEvent);
		boutonValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Fenetre.boutonLancer.setEnabled(true);
				Fenetre.boutonPause.setEnabled(false);
				Fenetre.lancerEnable = true;
				Fenetre.pauseEnable = false;
				Fenetre.champTemps.setText("Jeu en Pause");

				dispose();
			}
		});
		orangeBouton.addMouseListener(gestEvent);
		orangeBouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Fenetre.couleurPrincipale = "orange";
				Fenetre.setCouleurEchiquier();
			}
		});
		vertBouton.addMouseListener(gestEvent);
		vertBouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Fenetre.couleurPrincipale = "vert";
				Fenetre.setCouleurEchiquier();
			}
		});
		marronBouton.addMouseListener(gestEvent);
		marronBouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Fenetre.couleurPrincipale = "marron";
				Fenetre.setCouleurEchiquier();
			}
		});
		bleuBouton.addMouseListener(gestEvent);
		bleuBouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Fenetre.couleurPrincipale = "bleu";
				Fenetre.setCouleurEchiquier();
			}
		});
		violetBouton.addMouseListener(gestEvent);
		violetBouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Fenetre.couleurPrincipale = "violet";
				Fenetre.setCouleurEchiquier();
			}
		});
		grisBouton.addMouseListener(gestEvent);
		grisBouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Fenetre.couleurPrincipale = "gris";
				Fenetre.setCouleurEchiquier();
			}
		});
		turquoiseBouton.addMouseListener(gestEvent);
		turquoiseBouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Fenetre.couleurPrincipale = "turquoise";
				Fenetre.setCouleurEchiquier();
			}
		});
		rougeBouton.addMouseListener(gestEvent);
		rougeBouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Fenetre.couleurPrincipale = "rouge";
				Fenetre.setCouleurEchiquier();
			}
		});

		// Panel Principal.
		panelPrincipalParametres.setBackground(new Color(100, 100, 100));
		panelPrincipalParametres.setLayout(null);
		panelPrincipalParametres.add(panelTitre, null);
		panelPrincipalParametres.add(panelSonTitre, null);
		panelPrincipalParametres.add(panelEchiquierTitre, null);
		panelPrincipalParametres.add(boutonValider, null);
		panelPrincipalParametres.add(orangeBouton, null);
		panelPrincipalParametres.add(vertBouton, null);
		panelPrincipalParametres.add(marronBouton, null);
		panelPrincipalParametres.add(bleuBouton, null);
		panelPrincipalParametres.add(violetBouton, null);
		panelPrincipalParametres.add(grisBouton, null);
		panelPrincipalParametres.add(turquoiseBouton, null);
		panelPrincipalParametres.add(rougeBouton, null);

		// Panel Titre "PARAM�TRES".
		panelTitre.setBackground(new Color(100, 100, 100));
		panelTitre.setBounds(0, 0, 850, 70);
		panelTitre.add(labelTitre);
		labelTitre.setForeground(Color.white);
		labelTitre.setFont(policeTitre);

		// Panel gestion du son.
		panelSonTitre.setBackground(new Color(100, 100, 100));
		panelSonTitre.add(labelSon, null);
		// panelSonTitre.add(jtfNomJoueur1, null);
		panelSonTitre.setBounds(10, 70, 112, 100);
		labelSon.setHorizontalAlignment(JLabel.LEFT);
		labelSon.setForeground(Color.white);
		labelSon.setFont(policeLabels);

		// Panel choix de la couleur de l'�chiquier.
		panelEchiquierTitre.setBackground(new Color(100, 100, 100));
		panelEchiquierTitre.add(labelEchiquier, null);
		panelEchiquierTitre.setBounds(10, 235, 200, 40);
		labelEchiquier.setHorizontalAlignment(JLabel.LEFT);
		labelEchiquier.setForeground(Color.white);
		labelEchiquier.setFont(policeLabels);

		// Bouton couleur Orange.
		orangeBouton.setBounds(60, 285, 150, 150);
		orangeBouton.setBorderPainted(false);
		orangeBouton.setFocusPainted(false);

		// Bouton couleur Vert.
		vertBouton.setBounds(250, 285, 150, 150);
		vertBouton.setBorderPainted(false);
		vertBouton.setFocusPainted(false);

		// Bouton couleur Marron.
		marronBouton.setBounds(445, 285, 150, 150);
		marronBouton.setBorderPainted(false);
		marronBouton.setFocusPainted(false);

		// Bouton couleur Bleu.
		bleuBouton.setBounds(640, 285, 150, 150);
		bleuBouton.setBorderPainted(false);
		bleuBouton.setFocusPainted(false);

		// Bouton couleur Violet.
		violetBouton.setBounds(60, 460, 150, 150);
		violetBouton.setBorderPainted(false);
		violetBouton.setFocusPainted(false);

		// Bouton couleur Gris.
		grisBouton.setBounds(250, 460, 150, 150);
		grisBouton.setBorderPainted(false);
		grisBouton.setFocusPainted(false);

		// Bouton couleur Jaune.
		turquoiseBouton.setBounds(445, 460, 150, 150);
		turquoiseBouton.setBorderPainted(false);
		turquoiseBouton.setFocusPainted(false);

		// Bouton couleur Rouge.
		rougeBouton.setBounds(640, 460, 150, 150);
		rougeBouton.setBorderPainted(false);
		rougeBouton.setFocusPainted(false);

		// Bouton Valider.
		boutonValider.setBounds(335, 630, 180, 60);
		boutonValider.setBorderPainted(false);
		boutonValider.setFocusPainted(false);

		this.setVisible(true);
	}

	private class EvenementsSouris extends MouseAdapter {

		// Entr�e de la souris.
		public void mouseEntered(MouseEvent event) {
			if (event.getSource() == boutonValider) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 335, b = 630, c = 180, d = 60;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					boutonValider.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == orangeBouton) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 60, b = 285, c = 150, d = 150;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					orangeBouton.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == vertBouton) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 250, b = 285, c = 150, d = 150;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					vertBouton.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == marronBouton) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 445, b = 285, c = 150, d = 150;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					marronBouton.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == bleuBouton) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 640, b = 285, c = 150, d = 150;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					bleuBouton.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == violetBouton) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 60, b = 460, c = 150, d = 150;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					violetBouton.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == grisBouton) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 250, b = 460, c = 150, d = 150;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					grisBouton.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == turquoiseBouton) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 445, b = 460, c = 150, d = 150;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					turquoiseBouton.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == rougeBouton) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 640, b = 460, c = 150, d = 150;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					rougeBouton.setBounds(a, b, c, d);
				}
			}
		}

		// Sortie de la souris.
		public void mouseExited(MouseEvent event) {
			if (event.getSource() == boutonValider) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = false;
				int a = 330, b = 625, c = 190, d = 70;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					boutonValider.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == orangeBouton) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = true;
				int a = 55, b = 280, c = 160, d = 160;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					orangeBouton.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == vertBouton) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = true;
				int a = 245, b = 280, c = 160, d = 160;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					vertBouton.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == marronBouton) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = true;
				int a = 440, b = 280, c = 160, d = 160;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					marronBouton.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == bleuBouton) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = true;
				int a = 635, b = 280, c = 160, d = 160;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					bleuBouton.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == violetBouton) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = true;
				int a = 55, b = 455, c = 160, d = 160;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					violetBouton.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == grisBouton) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = true;
				int a = 245, b = 455, c = 160, d = 160;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					grisBouton.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == turquoiseBouton) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = true;
				int a = 440, b = 455, c = 160, d = 160;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					turquoiseBouton.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == rougeBouton) {
				setCursor(Cursor.getDefaultCursor());
				Fenetre.surLeBouton = true;
				int a = 635, b = 455, c = 160, d = 160;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					rougeBouton.setBounds(a, b, c, d);
				}
			}
		}

		// En train de cliquer sur le bouton.
		public void mousePressed(MouseEvent event) {
			if (event.getSource() == boutonValider) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = false;
				int a = 330, b = 625, c = 190, d = 70;
				for (int i = 1; i < 6; i++) {
					a = a + 1;
					b = b + 1;
					c = c - 2;
					d = d - 2;
					boutonValider.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == orangeBouton) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 55, b = 280, c = 160, d = 160;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					orangeBouton.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == vertBouton) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 245, b = 280, c = 160, d = 160;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					vertBouton.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == marronBouton) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 440, b = 280, c = 160, d = 160;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					marronBouton.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == bleuBouton) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 635, b = 280, c = 160, d = 160;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					bleuBouton.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == violetBouton) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 55, b = 455, c = 160, d = 160;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					violetBouton.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == grisBouton) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 245, b = 455, c = 160, d = 160;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					grisBouton.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == turquoiseBouton) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 440, b = 455, c = 160, d = 160;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					turquoiseBouton.setBounds(a, b, c, d);
				}
			} else if (event.getSource() == rougeBouton) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				Fenetre.surLeBouton = true;
				int a = 635, b = 455, c = 160, d = 160;
				for (int i = 1; i < 6; i++) {
					a = a - 1;
					b = b - 1;
					c = c + 2;
					d = d + 2;
					rougeBouton.setBounds(a, b, c, d);
				}
			}
		}

		// Relacher le clic du bouton.
		public void mouseReleased(MouseEvent event) {
			if (event.getSource() == boutonValider) {
				if (event.getX() > 0 && event.getX() < boutonValider.getWidth() && event.getY() > 0
						&& event.getY() < boutonValider.getHeight()) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					Fenetre.surLeBouton = true;
					int a = 335, b = 630, c = 180, d = 60;
					for (int i = 1; i < 6; i++) {
						a = a - 1;
						b = b - 1;
						c = c + 2;
						d = d + 2;
						boutonValider.setBounds(a, b, c, d);
					}
				}
			} else if (event.getSource() == orangeBouton) {
				if (event.getX() > 0 && event.getX() < orangeBouton.getWidth() && event.getY() > 0
						&& event.getY() < orangeBouton.getHeight()) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					Fenetre.surLeBouton = true;
					int a = 60, b = 285, c = 150, d = 150;
					for (int i = 1; i < 6; i++) {
						a = a - 1;
						b = b - 1;
						c = c + 2;
						d = d + 2;
						orangeBouton.setBounds(a, b, c, d);
					}
				}
			} else if (event.getSource() == vertBouton) {
				if (event.getX() > 0 && event.getX() < vertBouton.getWidth() && event.getY() > 0
						&& event.getY() < vertBouton.getHeight()) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					Fenetre.surLeBouton = true;
					int a = 250, b = 285, c = 150, d = 150;
					for (int i = 1; i < 6; i++) {
						a = a - 1;
						b = b - 1;
						c = c + 2;
						d = d + 2;
						vertBouton.setBounds(a, b, c, d);
					}
				}
			} else if (event.getSource() == marronBouton) {
				if (event.getX() > 0 && event.getX() < marronBouton.getWidth() && event.getY() > 0
						&& event.getY() < marronBouton.getHeight()) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					Fenetre.surLeBouton = true;
					int a = 445, b = 285, c = 150, d = 150;
					for (int i = 1; i < 6; i++) {
						a = a - 1;
						b = b - 1;
						c = c + 2;
						d = d + 2;
						marronBouton.setBounds(a, b, c, d);
					}
				}
			} else if (event.getSource() == bleuBouton) {
				if (event.getX() > 0 && event.getX() < bleuBouton.getWidth() && event.getY() > 0
						&& event.getY() < bleuBouton.getHeight()) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					Fenetre.surLeBouton = true;
					int a = 640, b = 285, c = 150, d = 150;
					for (int i = 1; i < 6; i++) {
						a = a - 1;
						b = b - 1;
						c = c + 2;
						d = d + 2;
						bleuBouton.setBounds(a, b, c, d);
					}
				}
			} else if (event.getSource() == violetBouton) {
				if (event.getX() > 0 && event.getX() < violetBouton.getWidth() && event.getY() > 0
						&& event.getY() < violetBouton.getHeight()) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					Fenetre.surLeBouton = true;
					int a = 60, b = 460, c = 150, d = 150;
					for (int i = 1; i < 6; i++) {
						a = a - 1;
						b = b - 1;
						c = c + 2;
						d = d + 2;
						violetBouton.setBounds(a, b, c, d);
					}
				}
			} else if (event.getSource() == grisBouton) {
				if (event.getX() > 0 && event.getX() < grisBouton.getWidth() && event.getY() > 0
						&& event.getY() < grisBouton.getHeight()) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					Fenetre.surLeBouton = true;
					int a = 250, b = 460, c = 150, d = 150;
					for (int i = 1; i < 6; i++) {
						a = a - 1;
						b = b - 1;
						c = c + 2;
						d = d + 2;
						grisBouton.setBounds(a, b, c, d);
					}
				}
			} else if (event.getSource() == turquoiseBouton) {
				if (event.getX() > 0 && event.getX() < turquoiseBouton.getWidth() && event.getY() > 0
						&& event.getY() < turquoiseBouton.getHeight()) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					Fenetre.surLeBouton = true;
					int a = 445, b = 460, c = 150, d = 150;
					for (int i = 1; i < 6; i++) {
						a = a - 1;
						b = b - 1;
						c = c + 2;
						d = d + 2;
						turquoiseBouton.setBounds(a, b, c, d);
					}
				}
			} else if (event.getSource() == rougeBouton) {
				if (event.getX() > 0 && event.getX() < rougeBouton.getWidth() && event.getY() > 0
						&& event.getY() < rougeBouton.getHeight()) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					Fenetre.surLeBouton = true;
					int a = 640, b = 460, c = 150, d = 150;
					for (int i = 1; i < 6; i++) {
						a = a - 1;
						b = b - 1;
						c = c + 2;
						d = d + 2;
						rougeBouton.setBounds(a, b, c, d);
					}
				}
			}
		}

		// Clic de la souris.
		public void mouseClicked(MouseEvent event) {
		}
	}
}
