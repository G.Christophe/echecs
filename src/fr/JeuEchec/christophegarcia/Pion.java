package fr.JeuEchec.christophegarcia;

public class Pion extends Piece {
	String couleur;

	public Pion(String couleur) {
		super("pion", couleur);
		this.couleur = couleur;
	}

	public boolean estValide(Deplacement deplacement) {
		if (couleur == "blanc") {
			if ((deplacement.getDeplacementX() == 0 && deplacement.getDeplacementY() == -1) // Avancement du pion de 1
																							// case
					|| (deplacement.getDepart().getLigne() == 6 && deplacement.getDeplacementY() == -2 // Avancement du
																										// pion de 2
																										// cases lors du
																										// premier coup
							&& deplacement.getDeplacementX() == 0)
					|| (Fenetre.priseEnPassantPossible // Prise en passant
							&& (deplacement.getDeplacementX() == -1 || deplacement.getDeplacementX() == 1)
							&& deplacement.getDeplacementY() == -1)
					|| ((deplacement.getDeplacementX() == -1 || deplacement.getDeplacementX() == 1) // Capture du pion
							&& deplacement.getDeplacementY() == -1
							&& Fenetre.echiquier
									.getCase(deplacement.getArrivee().getColonne(), deplacement.getArrivee().getLigne())
									.estOccupee("noir"))) {
				return true;
			}
		} else {
			if ((deplacement.getDeplacementX() == 0 && deplacement.getDeplacementY() == 1) // Avancement du pion de 1
																							// case
					|| (deplacement.getDepart().getLigne() == 1 && deplacement.getDeplacementY() == 2 // Avancement du
																										// pion de 2
																										// cases lors du
																										// premier coup
							&& deplacement.getDeplacementX() == 0)
					|| (Fenetre.priseEnPassantPossible // Prise en passant
							&& (deplacement.getDeplacementX() == -1 || deplacement.getDeplacementX() == 1))
							&& (deplacement.getDeplacementY() == 1)
					|| ((deplacement.getDeplacementX() == -1 || deplacement.getDeplacementX() == 1) // Capture du pion
							&& deplacement.getDeplacementY() == +1
							&& Fenetre.echiquier
									.getCase(deplacement.getArrivee().getColonne(), deplacement.getArrivee().getLigne())
									.estOccupee("blanc"))) {
				return true;
			}
		}
		return false;
	}
}
