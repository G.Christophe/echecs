package fr.JeuEchec.christophegarcia;

public class Deplacement {
	private Position depart;
	private Position arrivee;
	private double deplacementX;
	private double deplacementY;
	
	public Deplacement(Position depart, Position arrivee) {
		this.depart = depart;
		this.arrivee = arrivee;
		deplacementX = arrivee.getColonne() - depart.getColonne();
		deplacementY = arrivee.getLigne() - depart.getLigne();
	}
	
	public Position getDepart() {
		return depart;
	}
	
	public Position getArrivee() {
		return arrivee;
	}
	
	public double getDeplacementX() {
		return deplacementX;
	}
	
	public double getDeplacementY() {
		return deplacementY;
	}
	
	public boolean noDep() {
		return deplacementX == 0 && deplacementY == 0;
	}
}
