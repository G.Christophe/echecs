package fr.JeuEchec.christophegarcia;

public abstract class Piece {
	private String nom;
	protected String couleur;
	
	public Piece(String nom, String couleur) {
		setNom(nom);
		setCouleur(couleur);
	}
	
	public String getNom() {
	    return nom;
	}

	public String getCouleur() {
	    return couleur;
	}

	public void setNom(String nom) {
	    if(nom == "tour" || nom == "cavalier" || nom == "fou" || nom == "reine" || nom == "roi" || nom == "pion") {
	    	this.nom = nom;
	    }
	}

	public void setCouleur(String couleur) {
	    if(couleur == "blanc" || couleur == "noir") {
	    	this.couleur = couleur;
	    }
	}
	
	public abstract boolean estValide(Deplacement deplacement);
}
